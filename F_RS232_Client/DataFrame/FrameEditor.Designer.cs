﻿namespace F_RS232_Client.DataFrame
{
    partial class FrameEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.headPanel = new System.Windows.Forms.Panel();
            this.endSequenceTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataNameTextBox = new System.Windows.Forms.TextBox();
            this.dataSizeTextBox = new System.Windows.Forms.TextBox();
            this.dataTypeComboBox = new System.Windows.Forms.ComboBox();
            this.addButton = new System.Windows.Forms.Button();
            this.setEndSButton = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.cancelFrameButton = new System.Windows.Forms.Button();
            this.loadFrameButton = new System.Windows.Forms.Button();
            this.saveFrameButton = new System.Windows.Forms.Button();
            this.frameSaveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.frameOpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.headPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // headPanel
            // 
            this.headPanel.Controls.Add(this.groupBox1);
            this.headPanel.Controls.Add(this.endSequenceTextBox);
            this.headPanel.Controls.Add(this.label4);
            this.headPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.headPanel.Location = new System.Drawing.Point(0, 0);
            this.headPanel.Name = "headPanel";
            this.headPanel.Size = new System.Drawing.Size(292, 94);
            this.headPanel.TabIndex = 0;
            // 
            // endSequenceTextBox
            // 
            this.endSequenceTextBox.Location = new System.Drawing.Point(92, 68);
            this.endSequenceTextBox.Name = "endSequenceTextBox";
            this.endSequenceTextBox.Size = new System.Drawing.Size(191, 20);
            this.endSequenceTextBox.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(4, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "End sequence:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(177, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Size:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Type:";
            // 
            // dataNameTextBox
            // 
            this.dataNameTextBox.Location = new System.Drawing.Point(4, 34);
            this.dataNameTextBox.Name = "dataNameTextBox";
            this.dataNameTextBox.Size = new System.Drawing.Size(79, 20);
            this.dataNameTextBox.TabIndex = 0;
            this.dataNameTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dataNameTextBox_KeyPress);
            // 
            // dataSizeTextBox
            // 
            this.dataSizeTextBox.Location = new System.Drawing.Point(180, 35);
            this.dataSizeTextBox.Name = "dataSizeTextBox";
            this.dataSizeTextBox.ReadOnly = true;
            this.dataSizeTextBox.Size = new System.Drawing.Size(45, 20);
            this.dataSizeTextBox.TabIndex = 2;
            // 
            // dataTypeComboBox
            // 
            this.dataTypeComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dataTypeComboBox.FormattingEnabled = true;
            this.dataTypeComboBox.Location = new System.Drawing.Point(89, 34);
            this.dataTypeComboBox.Name = "dataTypeComboBox";
            this.dataTypeComboBox.Size = new System.Drawing.Size(82, 21);
            this.dataTypeComboBox.TabIndex = 1;
            this.dataTypeComboBox.SelectedIndexChanged += new System.EventHandler(this.dataTypeComboBox_SelectedIndexChanged);
            // 
            // addButton
            // 
            this.addButton.Location = new System.Drawing.Point(231, 33);
            this.addButton.Name = "addButton";
            this.addButton.Size = new System.Drawing.Size(49, 23);
            this.addButton.TabIndex = 0;
            this.addButton.Text = "Add";
            this.addButton.UseVisualStyleBackColor = true;
            this.addButton.Click += new System.EventHandler(this.addButton_Click);
            // 
            // setEndSButton
            // 
            this.setEndSButton.Location = new System.Drawing.Point(3, 3);
            this.setEndSButton.Name = "setEndSButton";
            this.setEndSButton.Size = new System.Drawing.Size(55, 23);
            this.setEndSButton.TabIndex = 7;
            this.setEndSButton.Text = "OK";
            this.setEndSButton.UseVisualStyleBackColor = true;
            this.setEndSButton.Click += new System.EventHandler(this.setEndSButton_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.AutoScroll = true;
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 94);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(292, 322);
            this.mainPanel.TabIndex = 0;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.cancelFrameButton);
            this.bottomPanel.Controls.Add(this.setEndSButton);
            this.bottomPanel.Controls.Add(this.loadFrameButton);
            this.bottomPanel.Controls.Add(this.saveFrameButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 416);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(292, 28);
            this.bottomPanel.TabIndex = 8;
            // 
            // cancelFrameButton
            // 
            this.cancelFrameButton.Location = new System.Drawing.Point(64, 2);
            this.cancelFrameButton.Name = "cancelFrameButton";
            this.cancelFrameButton.Size = new System.Drawing.Size(55, 23);
            this.cancelFrameButton.TabIndex = 8;
            this.cancelFrameButton.Text = "Cancel";
            this.cancelFrameButton.UseVisualStyleBackColor = true;
            this.cancelFrameButton.Click += new System.EventHandler(this.cancelFrameButton_Click);
            // 
            // loadFrameButton
            // 
            this.loadFrameButton.Location = new System.Drawing.Point(213, 2);
            this.loadFrameButton.Name = "loadFrameButton";
            this.loadFrameButton.Size = new System.Drawing.Size(75, 23);
            this.loadFrameButton.TabIndex = 10;
            this.loadFrameButton.Text = "Load frame";
            this.loadFrameButton.UseVisualStyleBackColor = true;
            this.loadFrameButton.Click += new System.EventHandler(this.loadFrameButton_Click);
            // 
            // saveFrameButton
            // 
            this.saveFrameButton.Location = new System.Drawing.Point(132, 2);
            this.saveFrameButton.Name = "saveFrameButton";
            this.saveFrameButton.Size = new System.Drawing.Size(75, 23);
            this.saveFrameButton.TabIndex = 9;
            this.saveFrameButton.Text = "Save frame";
            this.saveFrameButton.UseVisualStyleBackColor = true;
            this.saveFrameButton.Click += new System.EventHandler(this.saveFrameButton_Click);
            // 
            // frameOpenFileDialog
            // 
            this.frameOpenFileDialog.FileName = "openFileDialog1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.addButton);
            this.groupBox1.Controls.Add(this.dataTypeComboBox);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.dataSizeTextBox);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.dataNameTextBox);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(3, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(286, 64);
            this.groupBox1.TabIndex = 11;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "New variable";
            // 
            // FrameEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 444);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.headPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FrameEditor";
            this.Text = "FrameEditor";
            this.headPanel.ResumeLayout(false);
            this.headPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel headPanel;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox dataNameTextBox;
        private System.Windows.Forms.TextBox dataSizeTextBox;
        private System.Windows.Forms.ComboBox dataTypeComboBox;
        private System.Windows.Forms.Button addButton;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Button setEndSButton;
        private System.Windows.Forms.TextBox endSequenceTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Button cancelFrameButton;
        private System.Windows.Forms.Button saveFrameButton;
        private System.Windows.Forms.Button loadFrameButton;
        private System.Windows.Forms.SaveFileDialog frameSaveFileDialog;
        private System.Windows.Forms.OpenFileDialog frameOpenFileDialog;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}