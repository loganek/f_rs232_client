﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using F_RS232_Client.Options;

namespace F_RS232_Client
{
    public partial class MainForm : Form
    {
        const int MAX_SEND_BUFFER = 10000;
        const int MAX_RECEIVE_BUFFER = 10000;
        const int MAX_FRAME_BUFFER = 100;
        bool delTextPrevState; //is delTextafterSending checked?(Necessary with circular sending)
        RS232 spPort = new RS232();
        ConfigWrapper cfgWrapper = new ConfigWrapper(new ConfigManager("config.ini"));
        ConfigManager cfgManager = new ConfigManager("config.ini");
        MessageLogger msgLogger;
        DataFrame.Frame dataFrame;
        Macros.MacroList userMacroList = new Macros.MacroList();
        bool recRefreshByTimerEnabled = false;
        bool sendRefreshByTimerEnabled = false;
        string receivedText = "";
        string sendingText = "";
        List<ListViewItem> tmpListViewItem = new List<ListViewItem>();


        #region Options
        private void GetConnectionParams()
        {
            rescanButton_Click(null, null);
            cfgWrapper.GetComboBox("port", portyComboBox);
            cfgWrapper.GetComboBox("parity", parityComboBox);
            cfgWrapper.GetComboBox("stop_bits", stopBitsComboBox);
            cfgWrapper.GetComboBox("baud_rate", baudComboBox);
            cfgWrapper.GetComboBox("data_bits", dataBitsComboBox);
        }

        private void SetConnectionParams()
        {
            cfgWrapper.SetComboBox("port", portyComboBox);
            cfgWrapper.SetComboBox("parity", parityComboBox);
            cfgWrapper.SetComboBox("stop_bits", stopBitsComboBox);
            cfgWrapper.SetComboBox("baud_rate", baudComboBox);
            cfgWrapper.SetComboBox("data_bits", dataBitsComboBox);
        }

        private void MakeOptionChanges()
        {
            try
            {
                if (Convert.ToBoolean(cfgManager["RecTimeoutUpdate"]))
                {
                    recRefreshByTimerEnabled = true;
                    recRefreshControlsTimer.Interval = Convert.ToInt32(cfgManager["RecUpdateDataInterval"]);
                    if (spPort!= null && spPort.IsConnected)
                        recRefreshControlsTimer.Start();
                }
                else
                {
                    recRefreshByTimerEnabled = false;
                    recRefreshControlsTimer.Stop();
                }

                if (Convert.ToBoolean(cfgManager["SendTimeoutUpdate"]))
                {
                    sendRefreshByTimerEnabled = true;
                    sendRefreshControlsTimer.Interval = Convert.ToInt32(cfgManager["SendUpdateDataInterval"]);
                    if (spPort != null && spPort.IsConnected)
                        sendRefreshControlsTimer.Start();
                }
                else
                {
                    sendRefreshByTimerEnabled = false;
                    sendRefreshControlsTimer.Stop();
                }
            }
            catch (Exception ex)
            {
                if (MessageBox.Show("Cannot load configuration: " + ex.Message + "\n Would you like to clear configuration file?", "Error", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    cfgManager.ResetConfig();
                    cfgManager.LoadConfig();
                    MakeOptionChanges();
                }
            }
        }

        private void optionsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OptionsForm opForm = new OptionsForm();
            opForm.LoadConfig(cfgManager);
            if (opForm.ShowDialog() == DialogResult.OK)
            {
                cfgManager = opForm.CfgManager;
                MakeOptionChanges();
            }
        }

        public void LoadDefaultFiles()
        {
            if (System.IO.File.Exists(cfgManager["MacroDefaultFile"]))
                userMacroList.LoadFromFile(cfgManager["MacroDefaultFile"]);
            
            if (System.IO.File.Exists(cfgManager["FrameDefaultFile"]))
                dataFrame.LoadFromFile(cfgManager["FrameDefaultFile"]);
        }
#endregion Options

        public MainForm()
        {
            InitializeComponent();
            System.Reflection.AssemblyName asName = System.Reflection.Assembly.GetExecutingAssembly().GetName();
            this.Text = asName.Name + " "
#if DEBUG
            + asName.Version.ToString();
#else
            + asName.Version.Major.ToString() + "." + asName.Version.Minor.ToString();
#endif

            msgLogger = new MessageLogger(sendingRichTextBox);
            dataFrame = new DataFrame.Frame();
            dataFrame.ChangedElementFrame += delegate(object sender, EventArgs e) { UpdateFrameListView(); };
            dataFrame.FrameReady += new DataFrame.FrameReadyEventHandler(dataFrame_FrameReady);
            DataFrame.ElementType.MaxElementCount = MAX_FRAME_BUFFER;

            cfgManager.LoadConfig();
            GetConnectionParams();
            MakeOptionChanges();
            LoadDefaultFiles();

            spPort.Connected += new ConnectedEventHandler(spPort_Connected);
            spPort.Disconnected += new DisconnectedEventHandler(spPort_Disconnected);
            spPort.DataReceived += new DataReceivedEventHandler(spPort_DataReceived);
            spPort.DataSent += new DataSentEventHandler(spPort_DataSent);

            clearFrameValuesButton.DataBindings.Add("Visible", receiveSplitContainer.Panel2, "Visible");
        }

        private void rescanButton_Click(object sender, EventArgs e)
        {
            portyComboBox.DataSource = RS232.GetAvaliablePorts();
        }

        #region Connection
        private void connectButton_Click(object sender, EventArgs e)
        {
            try
            {
                spPort.SetConnectionParameters(portyComboBox.SelectedItem.ToString(),
                    Convert.ToInt32(baudComboBox.SelectedItem),
                    RS232.ParityArray(parityComboBox.SelectedIndex),
                    Convert.ToInt32(dataBitsComboBox.SelectedItem),
                    RS232.StopBitsArray(stopBitsComboBox.SelectedIndex));

                spPort.Connect();

                if (recRefreshByTimerEnabled)
                    recRefreshControlsTimer.Start();
                if (sendRefreshByTimerEnabled)
                    sendRefreshControlsTimer.Start();

                msgLogger.WriteLog("Port " + spPort.PortName + " opened", LogType.HappyLog);


                SetConnectionParams();
                cfgWrapper.SaveConfig();
            }
            catch (Exception ex)
            {
                msgLogger.WriteLog("Cannot connect to port: " + ex.Message, LogType.ErrorLog);
            }
        }

        private void disconnectButton_Click(object sender, EventArgs e)
        {
            if (!spPort.Disconnect())
                msgLogger.WriteLog("Cannot close port", LogType.ErrorLog);
            else
            {
                recRefreshControlsTimer.Stop();
                sendRefreshControlsTimer.Stop();
                msgLogger.WriteLog("Port " + spPort.PortName + " closed", LogType.HappyLog);
            }
        }

        private void spPort_Connected(object sender, EventArgs e)
        {
            connectButton.Text = "Disconnect";
            connectButton.Click -= connectButton_Click;
            connectButton.Click += disconnectButton_Click;
            sendButton.Enabled = true;
            circularSendingCheckBox.Enabled = true;
        }


        private void spPort_Disconnected(object sender, EventArgs e)
        {
            connectButton.Text = "Connect";
            connectButton.Click -= disconnectButton_Click;
            connectButton.Click += connectButton_Click;
            sendButton.Enabled = false;
            circularSendingCheckBox.Checked = false; ;
            circularSendingCheckBox.Enabled = false;
        }
        #endregion Connection

        #region Sending
        private void UpdateSendingRichTextBoxInside(string texter = null)
        {
            sendingRichTextBox.SelectionStart = sendingRichTextBox.Text.Length;
            sendingRichTextBox.ScrollToCaret();

            if (sendingRichTextBox.TextLength > MAX_SEND_BUFFER)
                sendingRichTextBox.Clear();

            if (sendRefreshByTimerEnabled)
            {
                sendingRichTextBox.AppendText(sendingText);
                sendingText = "";
            }
            else
            {
                sendingRichTextBox.AppendText(texter);
            }
            sendingRichTextBox.SelectionStart = sendingRichTextBox.Text.Length;
            sendingRichTextBox.ScrollToCaret();
            
            if (newLineSendCheckBox.Checked)
                sendingRichTextBox.AppendText("\r\n");
        }

        public void UpdateSendingRichTextBox(string texter = null)
        {
            if (sendingRichTextBox.InvokeRequired)
                sendingRichTextBox.Invoke(new Action(() =>
                {
                    UpdateSendingRichTextBoxInside(texter);
                }));
            else if (texter == null)
                UpdateSendingRichTextBoxInside();
            else
                UpdateSendingRichTextBoxInside(texter);
        }


        private void clearSentButton_Click(object sender, EventArgs e)
        {
            sendingRichTextBox.Clear();
        }

        private int SendedChars()
        {
            if (hexSendCharsRadioButton.Checked)
                return 16;
            else
                return -1;
        }

        private void sendButton_Click(object sender, EventArgs e)
        {
            if (spPort != null && spPort.IsConnected)
            {
                try
                {
                    List<byte> bufList = StringParser.StrToByteArray(sendTextBox.Text).ToList<byte>();

                    if (plusCRCheckBox.Checked)
                        bufList.Add(13);
                    if (plusLFCheckBox.Checked)
                        bufList.Add(10);

                    byte[] buf = bufList.ToArray();

                    spPort.Send(buf);

                    if (delTextCheckBox.Checked)
                        sendTextBox.Text = "";
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot send data: " + ex.Message);
                }
            }
        }

       
        private void spPort_DataSent(object sender, SentEventArgs e)
        {
            try
            {
                if (!sendRefreshByTimerEnabled)
                    UpdateSendingRichTextBox(StringParser.ByteToDisplay(e.Buff, SendedChars()));
                else
                {
                    if (sendingText.Length > MAX_SEND_BUFFER)
                        sendingText = "";

                    sendingText += StringParser.ByteToDisplay(e.Buff, SendedChars());
                }
            }
            catch (Exception ex)
            {
                msgLogger.WriteLog("Exception: " + ex.Message);
            }
        }

        private void circularSendingTimer_Tick(object sender, EventArgs e)
        {
            sendButton_Click(sender, null);
        }

        private void circularSendingCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (circularSendingCheckBox.Checked)
            {
                int interval = 0;
                try
                {
                    interval = Convert.ToInt32(circularTimeoutTextBox.Text);
                }
                catch
                {
                    interval = 1000;
                    circularTimeoutTextBox.Text = interval.ToString();
                }
                finally
                {
                    sendTextBox.Enabled = false;
                    circularTimeoutTextBox.ReadOnly = true;
                    circularSendingTimer.Interval = interval;
                    delTextPrevState = delTextCheckBox.Checked;
                    delTextCheckBox.Enabled = false;
                    delTextCheckBox.Checked = false;
                    circularSendingTimer.Start();
                }
            }
            else
            {
                circularSendingTimer.Stop();
                circularTimeoutTextBox.ReadOnly = false;
                sendTextBox.Enabled = true;
                delTextCheckBox.Enabled = true;
                delTextCheckBox.Checked = delTextPrevState;
            }
        }

        private void sendTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return && sendButton.Enabled)
            {
                sendButton_Click(sender, null);

                if (delTextCheckBox.Checked)
                    sendTextBox.Text = "";
            }
        }
        #endregion Sending

        #region Receiving

        private void UpdateReceiveRichTextBoxInside(string texter = null)
        {
            receiveRichTextBox.Select(Math.Max(receiveRichTextBox.TextLength - 1, 0), 0);

            if (receiveRichTextBox.TextLength > MAX_RECEIVE_BUFFER)
                receiveRichTextBox.Clear();

            if (recRefreshByTimerEnabled)
            {
                receiveRichTextBox.AppendText(receivedText);
                receivedText = "";
            }
            else
            {
                receiveRichTextBox.AppendText(texter);
            }
            receiveRichTextBox.SelectionStart = receiveRichTextBox.Text.Length;
            receiveRichTextBox.ScrollToCaret();
        }

        public void UpdateReceivedRichTextBox(string texter = null)
        {
            if (receiveRichTextBox.InvokeRequired)
                receiveRichTextBox.Invoke(new Action(() =>
                {
                    UpdateReceiveRichTextBoxInside(texter);
                }));
            else if (texter == null)
                UpdateReceiveRichTextBoxInside();
            else
                UpdateReceiveRichTextBoxInside(texter);
        }

        private void spPort_DataReceived(object sender, EventArgs e)
        {
            try
            {
                dataFrame.AppendToFrame(spPort.RsData);
            }
            catch (Exception ex)
            {
                msgLogger.WriteLog("Exception: " + ex.Message);
            }
            if (!recRefreshByTimerEnabled)
            {
                UpdateReceivedRichTextBox(StringParser.ByteToDisplay(spPort.RsData, (hexReceiveDisplayModeRadioButton.Checked) ? 16 : -1));
            }
            else
            {
                if (receivedText.Length > MAX_RECEIVE_BUFFER)
                    receivedText = "";

                receivedText += StringParser.ByteToDisplay(spPort.RsData, (hexReceiveDisplayModeRadioButton.Checked) ? 16 : -1);
            }
        }

        private void clearReceivedButton_Click(object sender, EventArgs e)
        {
            receiveRichTextBox.Clear();
        }


        #endregion Receiving

        private void macroPluginButton_Click(object sender, EventArgs e)
        {
            Macros.MacroView mView = new Macros.MacroView();
            mView.LoadMacroList(userMacroList);
            mView.LoadRS(spPort);
            mView.Show();
            
            int left = this.Left + this.Width;

            if (Screen.PrimaryScreen.Bounds.Width - left < mView.Width)
                mView.Left = Screen.PrimaryScreen.Bounds.Width - mView.Width - 10;
            else
                mView.Left = left;

            mView.Top = this.Top + 10;
        }

        #region DataFrame

        private void AddElementsToListView()
        {
            string[] str = new string[dataFrame.ElementCount];
            for (int i = 0; i < dataFrame.ElementCount; i++)
            {
                if (dataFrame[i].TypeOf == DataFrame._ElementType.BYTE_ARRAY)
                {
                    string t = string.Empty;
                    byte[] array = (dataFrame[i].CurrentValue) as byte[];

                    if (array == null)
                        t = dataFrame[i].ToString();
                    else
                        for (int j = 0; j < array.Length; j++)
                            t += array[j].ToString() + " ";
                    str[i] = t;
                }
                else
                    str[i] = dataFrame[i].CurrentValue.ToString();
            }

            if (!recRefreshByTimerEnabled)
                UpdateReceivedListView(new ListViewItem(str));
            else
            {
                tmpListViewItem.Add(new ListViewItem(str));

                if (tmpListViewItem.Count > MAX_FRAME_BUFFER)
                    tmpListViewItem.RemoveAt(0);
            }
        }

        private void dataFrame_FrameReady(object sender, DataFrame.FrameReadyEventArgs e)
        {
            if (receivedFrameListView.InvokeRequired)
                receivedFrameListView.Invoke(new Action(() =>
               {
                   AddElementsToListView();
               }));
            else
                AddElementsToListView();
        }

         private void UpdateReceiveListViewInside(ListViewItem texter = null)
        {
            if (recRefreshByTimerEnabled)
            {
                for(int i = 0; i < tmpListViewItem.Count; i++)
                {
                    receivedFrameListView.Items.Add(tmpListViewItem[i]);
                }
                tmpListViewItem.Clear();
            }
            else
            {
                receivedFrameListView.Items.Add(texter);
            }
            
            if (receivedFrameListView.Items.Count > MAX_FRAME_BUFFER)
                receivedFrameListView.Items.RemoveAt(0);

             if(receivedFrameListView.Items.Count>0)
                receivedFrameListView.Items[receivedFrameListView.Items.Count - 1].EnsureVisible();
        }

         public void UpdateReceivedListView(ListViewItem texter = null)
         {
             if (receivedFrameListView.InvokeRequired)
                 receivedFrameListView.Invoke(new Action(() =>
                 {
                     UpdateReceiveListViewInside(texter);
                 }));
             else if (texter == null)
                 UpdateReceiveListViewInside();
             else
                 UpdateReceiveListViewInside(texter);
         }
           

        private void UpdateFrameListView()
        {
            receivedFrameListView.Columns.Clear();

            int width = 0;

            if(dataFrame.ElementCount > 0)
                width = receivedFrameListView.Width / dataFrame.ElementCount;

            for (int i = 0; i < dataFrame.ElementCount; i++)
                receivedFrameListView.Columns.Add(dataFrame[i].VarName, width);
        }

        private void frameEditorButton_Click(object sender, EventArgs e)
        {
            DataFrame.FrameEditor frEdView = new DataFrame.FrameEditor();
            frEdView.LoadFrame(dataFrame);
            frEdView.ShowDialog();
        }

        private void showFrameDataButton_Click(object sender, EventArgs e)
        {
            receiveSplitContainer.Panel2Collapsed = !receiveSplitContainer.Panel2Collapsed;
        }

        #endregion DataFrame

        private void receivedFrameListView_Resize(object sender, EventArgs e)
        {
            if (receivedFrameListView.Columns.Count == 0)
                return;

            int width = receivedFrameListView.Width / receivedFrameListView.Columns.Count;

            for (int i = 0; i < receivedFrameListView.Columns.Count; i++)
                receivedFrameListView.Columns[i].Width = width - 3;
        }

        private void graphButton_Click(object sender, EventArgs e)
        {
            Graph.GraphView gView = new Graph.GraphView();
            gView.LoadFrame(dataFrame);
            dataFrame.FrameReady += gView.pleaseDrawMeNow;
            gView.Show();
        }

        private void refreshControlsTimer_Tick(object sender, EventArgs e)
        {
            UpdateReceivedRichTextBox();
            UpdateReceivedListView();
        }

        
        private void sendRefreshControlsTimer_Tick(object sender, EventArgs e)
        {
            UpdateSendingRichTextBox();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new AboutWindowForm().ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    userMacroList.LoadFromFile(openFileDialog.FileName);
                    MessageBox.Show("Macros loaded");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot load macro: " + ex.Message);
                }
            }
        }

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    dataFrame.LoadFromFile(openFileDialog.FileName);
                    MessageBox.Show("Frame loaded");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot load frame: " + ex.Message);
                }
            }
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    userMacroList.SaveToFile(saveFileDialog.FileName);
                    MessageBox.Show("Macro list saved");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot save macro list: " + ex.Message);
                }
            }
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    dataFrame.SaveToFile(saveFileDialog.FileName);
                    MessageBox.Show("Frame saved");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot save frame: " + ex.Message);
                }
            }
        }

        private void clearFrameValuesButton_Click(object sender, EventArgs e)
        {
            receivedFrameListView.Items.Clear();
        }
    }
}
