﻿namespace F_RS232_Client.Graph
{
    partial class GraphView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.varListGroupBox = new System.Windows.Forms.GroupBox();
            this.varListPanel = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.graphCountComboBox = new System.Windows.Forms.ComboBox();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.leftSplitContainer = new System.Windows.Forms.SplitContainer();
            this.z1GraphControl = new ZedGraph.ZedGraphControl();
            this.z2GraphControl = new ZedGraph.ZedGraphControl();
            this.rightSplitContainer = new System.Windows.Forms.SplitContainer();
            this.z3GraphControl = new ZedGraph.ZedGraphControl();
            this.z4GraphControl = new ZedGraph.ZedGraphControl();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.varListGroupBox.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftSplitContainer)).BeginInit();
            this.leftSplitContainer.Panel1.SuspendLayout();
            this.leftSplitContainer.Panel2.SuspendLayout();
            this.leftSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rightSplitContainer)).BeginInit();
            this.rightSplitContainer.Panel1.SuspendLayout();
            this.rightSplitContainer.Panel2.SuspendLayout();
            this.rightSplitContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // varListGroupBox
            // 
            this.varListGroupBox.Controls.Add(this.varListPanel);
            this.varListGroupBox.Controls.Add(this.panel1);
            this.varListGroupBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.varListGroupBox.Location = new System.Drawing.Point(0, 0);
            this.varListGroupBox.Name = "varListGroupBox";
            this.varListGroupBox.Size = new System.Drawing.Size(217, 299);
            this.varListGroupBox.TabIndex = 2;
            this.varListGroupBox.TabStop = false;
            this.varListGroupBox.Text = "Variables";
            // 
            // varListPanel
            // 
            this.varListPanel.AutoScroll = true;
            this.varListPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.varListPanel.Location = new System.Drawing.Point(3, 16);
            this.varListPanel.Name = "varListPanel";
            this.varListPanel.Size = new System.Drawing.Size(211, 248);
            this.varListPanel.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.graphCountComboBox);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(3, 264);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(211, 32);
            this.panel1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Number of graphs:";
            // 
            // graphCountComboBox
            // 
            this.graphCountComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.graphCountComboBox.FormattingEnabled = true;
            this.graphCountComboBox.Location = new System.Drawing.Point(107, 8);
            this.graphCountComboBox.Name = "graphCountComboBox";
            this.graphCountComboBox.Size = new System.Drawing.Size(58, 21);
            this.graphCountComboBox.TabIndex = 0;
            this.graphCountComboBox.SelectedIndexChanged += new System.EventHandler(this.graphCountComboBox_SelectedIndexChanged);
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.mainSplitContainer.Name = "mainSplitContainer";
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.leftSplitContainer);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.rightSplitContainer);
            this.mainSplitContainer.Size = new System.Drawing.Size(527, 299);
            this.mainSplitContainer.SplitterDistance = 265;
            this.mainSplitContainer.TabIndex = 5;
            // 
            // leftSplitContainer
            // 
            this.leftSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leftSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.leftSplitContainer.Name = "leftSplitContainer";
            this.leftSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // leftSplitContainer.Panel1
            // 
            this.leftSplitContainer.Panel1.Controls.Add(this.z1GraphControl);
            // 
            // leftSplitContainer.Panel2
            // 
            this.leftSplitContainer.Panel2.Controls.Add(this.z2GraphControl);
            this.leftSplitContainer.Size = new System.Drawing.Size(265, 299);
            this.leftSplitContainer.SplitterDistance = 134;
            this.leftSplitContainer.TabIndex = 0;
            // 
            // z1GraphControl
            // 
            this.z1GraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.z1GraphControl.IsAutoScrollRange = true;
            this.z1GraphControl.IsShowHScrollBar = true;
            this.z1GraphControl.IsSynchronizeXAxes = true;
            this.z1GraphControl.Location = new System.Drawing.Point(0, 0);
            this.z1GraphControl.Name = "z1GraphControl";
            this.z1GraphControl.ScrollGrace = 0D;
            this.z1GraphControl.ScrollMaxX = 0D;
            this.z1GraphControl.ScrollMaxY = 0D;
            this.z1GraphControl.ScrollMaxY2 = 0D;
            this.z1GraphControl.ScrollMinX = 0D;
            this.z1GraphControl.ScrollMinY = 0D;
            this.z1GraphControl.ScrollMinY2 = 0D;
            this.z1GraphControl.Size = new System.Drawing.Size(265, 134);
            this.z1GraphControl.TabIndex = 2;
            // 
            // z2GraphControl
            // 
            this.z2GraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.z2GraphControl.IsAutoScrollRange = true;
            this.z2GraphControl.IsShowHScrollBar = true;
            this.z2GraphControl.IsSynchronizeXAxes = true;
            this.z2GraphControl.Location = new System.Drawing.Point(0, 0);
            this.z2GraphControl.Name = "z2GraphControl";
            this.z2GraphControl.ScrollGrace = 0D;
            this.z2GraphControl.ScrollMaxX = 0D;
            this.z2GraphControl.ScrollMaxY = 0D;
            this.z2GraphControl.ScrollMaxY2 = 0D;
            this.z2GraphControl.ScrollMinX = 0D;
            this.z2GraphControl.ScrollMinY = 0D;
            this.z2GraphControl.ScrollMinY2 = 0D;
            this.z2GraphControl.Size = new System.Drawing.Size(265, 161);
            this.z2GraphControl.TabIndex = 0;
            // 
            // rightSplitContainer
            // 
            this.rightSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rightSplitContainer.Location = new System.Drawing.Point(0, 0);
            this.rightSplitContainer.Name = "rightSplitContainer";
            this.rightSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // rightSplitContainer.Panel1
            // 
            this.rightSplitContainer.Panel1.Controls.Add(this.z3GraphControl);
            // 
            // rightSplitContainer.Panel2
            // 
            this.rightSplitContainer.Panel2.Controls.Add(this.z4GraphControl);
            this.rightSplitContainer.Size = new System.Drawing.Size(258, 299);
            this.rightSplitContainer.SplitterDistance = 134;
            this.rightSplitContainer.TabIndex = 0;
            // 
            // z3GraphControl
            // 
            this.z3GraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.z3GraphControl.IsAutoScrollRange = true;
            this.z3GraphControl.IsShowHScrollBar = true;
            this.z3GraphControl.IsSynchronizeXAxes = true;
            this.z3GraphControl.Location = new System.Drawing.Point(0, 0);
            this.z3GraphControl.Name = "z3GraphControl";
            this.z3GraphControl.ScrollGrace = 0D;
            this.z3GraphControl.ScrollMaxX = 0D;
            this.z3GraphControl.ScrollMaxY = 0D;
            this.z3GraphControl.ScrollMaxY2 = 0D;
            this.z3GraphControl.ScrollMinX = 0D;
            this.z3GraphControl.ScrollMinY = 0D;
            this.z3GraphControl.ScrollMinY2 = 0D;
            this.z3GraphControl.Size = new System.Drawing.Size(258, 134);
            this.z3GraphControl.TabIndex = 0;
            // 
            // z4GraphControl
            // 
            this.z4GraphControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.z4GraphControl.IsAutoScrollRange = true;
            this.z4GraphControl.IsShowHScrollBar = true;
            this.z4GraphControl.IsSynchronizeXAxes = true;
            this.z4GraphControl.Location = new System.Drawing.Point(0, 0);
            this.z4GraphControl.Name = "z4GraphControl";
            this.z4GraphControl.ScrollGrace = 0D;
            this.z4GraphControl.ScrollMaxX = 0D;
            this.z4GraphControl.ScrollMaxY = 0D;
            this.z4GraphControl.ScrollMaxY2 = 0D;
            this.z4GraphControl.ScrollMinX = 0D;
            this.z4GraphControl.ScrollMinY = 0D;
            this.z4GraphControl.ScrollMinY2 = 0D;
            this.z4GraphControl.Size = new System.Drawing.Size(258, 161);
            this.z4GraphControl.TabIndex = 0;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.varListGroupBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.mainSplitContainer);
            this.splitContainer1.Size = new System.Drawing.Size(748, 299);
            this.splitContainer1.SplitterDistance = 217;
            this.splitContainer1.TabIndex = 5;
            // 
            // GraphView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(748, 299);
            this.Controls.Add(this.splitContainer1);
            this.Name = "GraphView";
            this.Text = "GraphView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GraphView_FormClosing);
            this.varListGroupBox.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.leftSplitContainer.Panel1.ResumeLayout(false);
            this.leftSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.leftSplitContainer)).EndInit();
            this.leftSplitContainer.ResumeLayout(false);
            this.rightSplitContainer.Panel1.ResumeLayout(false);
            this.rightSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.rightSplitContainer)).EndInit();
            this.rightSplitContainer.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox varListGroupBox;
        private System.Windows.Forms.Panel varListPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox graphCountComboBox;
        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.SplitContainer leftSplitContainer;
        private ZedGraph.ZedGraphControl z1GraphControl;
        private ZedGraph.ZedGraphControl z2GraphControl;
        private System.Windows.Forms.SplitContainer rightSplitContainer;
        private ZedGraph.ZedGraphControl z3GraphControl;
        private ZedGraph.ZedGraphControl z4GraphControl;
        private System.Windows.Forms.SplitContainer splitContainer1;
    }
}