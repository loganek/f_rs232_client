﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace F_RS232_Client.DataFrame
{
    public partial class FrameEditor : Form
    {
        Frame realFrame = null;
        Frame fr = new Frame();

        public FrameEditor()
        {
            InitializeComponent();
            
            dataTypeComboBox.DataSource = System.Enum.GetValues(typeof(_ElementType));
            endSequenceTextBox.LostFocus += delegate(object sender, EventArgs e) { fr.EndSequence = StringParser.StrToByteArray(endSequenceTextBox.Text); };
        }

        public void LoadFrame(Frame f)
        {
            realFrame = f;
            fr.SetNewFrame(realFrame);
            DrawFrame();
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            try
            {
                fr.AddElement((_ElementType)dataTypeComboBox.SelectedValue, dataNameTextBox.Text, Convert.ToInt32(dataSizeTextBox.Text));

                dataNameTextBox.Clear();
                dataNameTextBox.Focus();
                DrawLine(fr.ElementCount - 1);

                if (fr.ElementCount > 1)
                    DrawDownButton(fr.ElementCount - 2);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void dataTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataSizeTextBox.Text = ElementType.ElementSize[dataTypeComboBox.SelectedIndex].ToString();
            dataSizeTextBox.ReadOnly = (_ElementType)dataTypeComboBox.SelectedValue != _ElementType.BYTE_ARRAY;
        }

        private void dataNameTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
                addButton_Click(sender, null);
        }

        #region Drawing
        private void DrawUpButton(int i)
        {
            Button btn = new Button();
            btn.Text = "^";
            btn.Tag += i.ToString();
            btn.Size = new Size(25, 25);
            btn.Location = new Point(200, 30 * i + 10);
            btn.Click += new EventHandler(moveUp_Click);
            mainPanel.Controls.Add(btn);
        }

        private void DrawDownButton(int i)
        {
            Button btn = new Button();
            btn.Text = "v";
            btn.Size = new Size(25, 25);
            btn.Tag += i.ToString();
            btn.Location = new Point(230, 30 * i + 10);
            btn.Click += new EventHandler(moveDown_Click);
            mainPanel.Controls.Add(btn);
        }

        private void DrawRemoveButton(int i)
        {
            Button btn = new Button();
            btn.Text = "X";
            btn.Size = new Size(25, 25);
            btn.Tag += i.ToString();
            btn.Location = new Point(260, 30 * i + 10);
            btn.Click += new EventHandler(remove_Click);
            mainPanel.Controls.Add(btn);
        }

        private void DrawLine(int i)
        {
            Label lbl = new Label();
            lbl.Text = fr[i].TypeOf.ToString() + " " + fr[i].VarName;

            if (fr[i].TypeOf == _ElementType.BYTE_ARRAY)
                lbl.Text += " [" + fr[i].Size.ToString() + "]";

            lbl.AutoSize = true;
            lbl.Location = new Point(10, 30 * i + 10);
            mainPanel.Controls.Add(lbl);

            if (i > 0)
                DrawUpButton(i);

            if (i < fr.ElementCount - 1)
                DrawDownButton(i);

            DrawRemoveButton(i);
        }

        private void DrawFrame()
        {
            if (fr == null)
                return;

            endSequenceTextBox.Text = StringParser.ByteArrayToStr(fr.EndSequence);

            while (mainPanel.Controls.Count > 0)
                mainPanel.Controls.RemoveAt(0);

            for (int i = 0; i < fr.ElementCount; i++)
            {
                DrawLine(i);
            }
        }
        #endregion Drawing

        private void moveUp_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            if (sender == null)
                return;

            int index = Convert.ToInt32(btn.Tag);

            fr.Swap(index, index - 1);
            DrawFrame();
        }

        private void moveDown_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            if (sender == null)
                return;

            int index = Convert.ToInt32(btn.Tag);

            fr.Swap(index, index + 1);
            DrawFrame();
        }

        private void remove_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            if (sender == null)
                return;

            int index = Convert.ToInt32(btn.Tag);

            fr.RemoveElement(index);
            DrawFrame();
        }


        private void setEndSButton_Click(object sender, EventArgs e)
        {
            try
            {
                fr.EndSequence = StringParser.StrToByteArray(endSequenceTextBox.Text);
                realFrame.SetNewFrame(fr);
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot set end sequence" + ex.Message);
            }
        }

        private void cancelFrameButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void saveFrameButton_Click(object sender, EventArgs e)
        {
            if (frameSaveFileDialog.ShowDialog() == DialogResult.OK)
                fr.SaveToFile(frameSaveFileDialog.FileName);
        }

        private void loadFrameButton_Click(object sender, EventArgs e)
        {
            if(frameOpenFileDialog.ShowDialog()== DialogResult.OK)
                if (System.IO.File.Exists(frameOpenFileDialog.FileName))
                {
                    fr.LoadFromFile(frameOpenFileDialog.FileName);
                    setEndSButton_Click(sender, e);
                }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
                this.Close();
            return base.ProcessCmdKey(ref msg, keyData);
        }
    }
}
