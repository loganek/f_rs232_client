﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ZedGraph;

namespace F_RS232_Client.Graph
{
    class GraphDataFrame
    {
        private DataFrame.ElementType eType = null;

        private PointPairList ppList = new PointPairList();
        private int iterator = 0;

        public int GraphID
        { set; get; }

        public int CurveID
        { set; get; }

        public System.Drawing.Color CurveColor
        { get; set; }

        public GraphDataFrame(DataFrame.ElementType et)
        {
            eType = et;
        }

        public string VarName
        {
            get { return eType.VarName; }
        }

        public DataFrame._ElementType TypeOf
        {
            get { return eType.TypeOf; }
        }

        public void AddPoint(double x)
        {
            ppList.Add(iterator++, x);
        }
        
    }
}
