﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace F_RS232_Client.Macros
{
    public partial class MacroView : Form
    {
        private MacroList macros = null;
        private RS232 rsPort = null;

        public Size ButtonSize
        { set; get; }

        public MacroView()
        {
            InitializeComponent();
            ButtonSize = new Size(40, 30);
            GenerateMacroView();
            TopMost = true;
        }

        public void LoadRS(RS232 rs)
        {
            rsPort = rs;
        }

        public void LoadMacroList(MacroList m)
        {
            macros = m;
            GenerateMacroView();
        }

        private void GenerateMacroView()
        {
            if (macros == null)
                return;

            while (mainPanel.Controls.Count > 0)
                mainPanel.Controls.RemoveAt(0);

            int sqrSize = (int)Math.Ceiling(Math.Sqrt((double)macros.Count));

            ClientSize = new Size(Math.Max(sqrSize * (ButtonSize.Width + 10) + 10, 120), Math.Max(sqrSize * (ButtonSize.Height + 10) + editorPanel.Height + 20 + hideFormButton.Height, 100));

            for (int i = 0; i < macros.Count; i++)
            {
                Button btn = new Button();
                btn.Size = ButtonSize;
                btn.Location = new Point(i % sqrSize * (ButtonSize.Width + 10) + 10, i / sqrSize * (ButtonSize.Height + 10) + 10);
                btn.Text = macros[i].Name;
                btn.Click += new EventHandler(macroButton_Click);
                mainPanel.Controls.Add(btn);
            }
        }

        private void macroButton_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;

            if (btn == null)
                return;

            try
            {
                if (macros[btn.Text] == null)
                    throw new Exception("Cannot find macro " + btn.Text + "in macrolist");

                if (rsPort != null)
                    rsPort.Send(StringParser.StrToByteArray(macros[btn.Text].Content));
                else
                    throw new Exception("RS232 object is empty!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error during send macro: "+ex.Message);
            }
        }


        private void editMacrosButton_Click(object sender, EventArgs e)
        {
            MacroEditor mEditor = new MacroEditor();
            mEditor.LoadList(macros);
            mEditor.ShowDialog();
            GenerateMacroView();
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape) this.Close();
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void hideFormButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
