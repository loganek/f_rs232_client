﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace F_RS232_Client.DataFrame
{
    public enum _ElementType { BYTE = 0, INT, DOUBLE, BYTE_ARRAY };

    public class ElementType
    {
        private _ElementType type;
        private int typeSize = 0;
        private static int[] elementSize = { 1, 4, 8, 1 };
        private List<object> valueList = new List<object>();
        private int totalCount = 0;

        public static int[] ElementSize
        {
            get { return elementSize; }
        }

        public static int MaxElementCount
        { set; get; }

        static ElementType()
        {
            MaxElementCount = 100;
        }

        public ElementType(_ElementType t, string varName, int size = 0)
        {
            VarName = varName;
            type = t;
            typeSize = (t == _ElementType.BYTE_ARRAY) ? size : elementSize[(int)t];
        }

        public string VarName
        { set; get; }

        public int TotalCount
        {
            get { return totalCount; }
        }

        public _ElementType TypeOf
        {
            get { return type; }
        }

        public List<object> Values
        {
            get { return valueList; }
        }

        public int StartIteratorValue()
        { return (MaxElementCount > totalCount) ? 0 : totalCount - MaxElementCount; }

        public object CurrentValue
        {
            set
            {
                valueList.Add(value);
                totalCount++;

                if (valueList.Count > MaxElementCount)
                    valueList.RemoveRange(0, valueList.Count - MaxElementCount);
            }

            get
            {
                return valueList.Last();
            }
        }

        public int Size { get { return typeSize; } }
    }
}
