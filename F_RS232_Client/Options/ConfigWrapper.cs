﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using F_RS232_Client.Options;

namespace F_RS232_Client
{
    class ConfigWrapper
    {
        ConfigManager cfgManager;

        public ConfigWrapper(ConfigManager cM)
        {
            cfgManager = cM;
            if (!cfgManager.IsLoaded())
                cfgManager.LoadConfig();
        }

        public void SaveConfig()
        {
            cfgManager.SaveConfig();
        }


        public void GetComboBox(string key, ComboBox cboBox)
        {
            try
            {
                cboBox.SelectedIndex = cfgManager.GetInt(key);
            }
            catch
            { }
        }

        public void SetComboBox(string key, ComboBox cboBox)
        {
            cfgManager[key] = cboBox.SelectedIndex.ToString();
        }
    }
}
