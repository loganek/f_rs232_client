﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace F_RS232_Client.Macros
{
    public class Macro
    {
        public string Name
        { set; get; }

        public string Content
        { set; get; }

        public Macro(string n, string c)
        {
            Name = n;
            Content = c;
        }
    }

    public class MacroList
    {
        private List<Macro> mList = new List<Macro>();

        public void AddMacro(string n, string c)
        {
            if (n == string.Empty)
                throw new Exception("Cannot add macro with empty name");

            if ((from x in mList
                 where x.Name == n
                 select x).Count() > 0)
                throw new Exception("Cannot add macro with the same name");

            mList.Add(new Macro(n, c));
        }

        public void RemoveMacro(string n)
        {
            mList.Remove((from x in mList where x.Name == n select x).First());
        }

        public void SetNewList(MacroList ml)
        {
            mList = ml.mList;
        }

        public int Count
        {
            get { return mList.Count; }
        }

        public Macro this[int index]
        {
            get { return mList[index]; }
        }

        public Macro this[string name]
        {
            get
            {
                return mList.Find(m => m.Name == name);
            }
        }

        public void LoadFromFile(string fileName)
        {
            StreamReader sr = new StreamReader(fileName);

            string line = sr.ReadLine();

            int macroCount = Convert.ToInt32(line);
            mList.Clear();

            for (int i = 0; i < macroCount; i++)
            {
                string code = sr.ReadLine();
                string name = sr.ReadLine();
                if (code == "")
                    continue;

                mList.Add(new Macro(name, code));
            }

            sr.Close();
        }

        public void SaveToFile(string fileName)
        {
            StreamWriter sw = new StreamWriter(fileName);

            sw.WriteLine(mList.Count.ToString());

            for (int i = 0; i < mList.Count; i++)
            {
                sw.WriteLine(mList[i].Content);
                sw.WriteLine(mList[i].Name);
            }

            sw.Close();
        }
    }
}
    