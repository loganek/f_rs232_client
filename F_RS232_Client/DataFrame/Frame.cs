﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace F_RS232_Client.DataFrame
{
    public static class ExtensionMethods
    {
        public static void Swap<T>(this List<T> list, int index1, int index2)
        {
            T tmp = list[index1];
            list[index1] = list[index2];
            list[index2] = tmp;
        }
    }

    public delegate void FrameReadyEventHandler(object sender, FrameReadyEventArgs e);

    public class FrameReadyEventArgs : EventArgs
    {
        private List<ElementType> elements;

        public List<ElementType> Elements
        {
            get { return elements; }
        }

        public FrameReadyEventArgs(List<ElementType> el)
        {
            elements = el;
        }
    }

    public class Frame
    {
        List<ElementType> elements = new List<ElementType>();
        byte[] endSequence = null;
        Queue<byte> receivedSequence = new Queue<byte>();
        int frameLength = 0;
        public event FrameReadyEventHandler FrameReady;
        public event EventHandler ChangedElementFrame;

        private static int MAX_QUEUE_SIZE = 10000;

        public Frame(byte[] endSeq = null)
        {
            EndSequence = endSeq;
        }

        protected virtual void OnFrameReady(FrameReadyEventArgs e)
        {
            if (FrameReady != null)
                FrameReady(this, e);
        }

        protected virtual void OnChangedFrameElement(EventArgs e)
        {
            if (ChangedElementFrame != null)
                ChangedElementFrame(this, e);
        }

        public void SetNewFrame(Frame fr)
        {
            EndSequence = fr.EndSequence;
            elements = new List<ElementType>(fr.elements);
            frameLength = fr.frameLength;
            OnChangedFrameElement(new EventArgs());
        }

        public int ElementCount
        {
            get { return elements.Count; }
        }

        public ElementType this[string index]
        {
            get { return elements.Find(x => x.VarName == index); }
        }

        public ElementType this[int index]
        {
            get { return elements[index]; }
        }

        public byte[] EndSequence
        {
            set
            {
                if ( endSequence != null)
                    frameLength -= endSequence.Length;

                endSequence = value;

                if (endSequence != null)
                    frameLength += endSequence.Length;
            }
            get
            {
                return endSequence;
            }
        }

        private bool IsEndSequence()
        {
            if (endSequence == null)
                return false;

            if (endSequence.Length > receivedSequence.Count)
                return false;

            for (int i = 1; i <= endSequence.Length; i++)
                if (endSequence[endSequence.Length - i] != receivedSequence.ElementAt<byte>(receivedSequence.Count - i)) 
                    return false;

            return true;
        }

        public void AddElement(_ElementType type, string varName, int size = 0)
        {
            if (varName.Contains(':') || varName.Contains('(') || varName.Contains(')') || varName==String.Empty)
                throw new Exception("Cannot use ':', '(' and ')' characters in variable name.\nVariable name cannot be empty");

            if ((from ElementType a in elements
                 where a.VarName == varName
                 select a).Count() > 0)
                throw new Exception("cannot add variable: variable exists");

            elements.Add(new ElementType(type, varName, size));
            frameLength += elements.Last<ElementType>().Size;

            OnChangedFrameElement(new EventArgs());
        }

        public void RemoveElement(int index)
        {
            RemoveElement(elements[index].VarName);
        }

        public void RemoveElement(string varName)
        {
            var el = (from ElementType a in elements
                     where a.VarName == varName
                     select a).First();

            if (el == null)
                throw new Exception("Cannot find element: " + varName);

            elements.Remove(el);
            frameLength -= el.Size;

            OnChangedFrameElement(new EventArgs());
        }

        public void AppendToFrame(byte b)
        {
            receivedSequence.Enqueue(b);
            if(receivedSequence.Count>MAX_QUEUE_SIZE)
            {
                int whileCond = MAX_QUEUE_SIZE/2;

                while (receivedSequence.Count > whileCond)
                    receivedSequence.Dequeue();
             }

            try
            {
                if (IsEndSequence())
                    Parse();
            }
            catch (Exception ex)
            {
                throw new Exception("Cannot parse data: " + ex.Message);
            }
        }

        public void AppendToFrame(byte[] buffer)
        {
            for (int i = 0; i < buffer.Length; i++)
                AppendToFrame(buffer[i]);
        }

        public void Swap(int i, int j)
        {
            elements.Swap(i, j);
            OnChangedFrameElement(new EventArgs());
        }

        void Parse()
        {
            if (receivedSequence.Count < frameLength)
            {
                receivedSequence.Clear();
                return;
            }

            while (receivedSequence.Count > frameLength)
                receivedSequence.Dequeue();

            int arrayIterator = 0;
            byte[] workingCopy = receivedSequence.ToArray();

            for (int i = 0; i < elements.Count; i++)
            {
                switch (elements[i].TypeOf)
                {
                    case _ElementType.INT:
                        elements[i].CurrentValue = BitConverter.ToInt32(workingCopy, arrayIterator);
                        break;
                    case _ElementType.BYTE:
                        elements[i].CurrentValue = workingCopy[arrayIterator];
                        break;
                    case _ElementType.DOUBLE:
                        elements[i].CurrentValue = BitConverter.ToDouble(workingCopy, arrayIterator);
                        break;
                    case _ElementType.BYTE_ARRAY:
                        elements[i].CurrentValue = new byte[elements[i].Size];
                        Buffer.BlockCopy(workingCopy, arrayIterator, (byte[])elements[i].CurrentValue, 0, elements[i].Size);
                        break;
                    default:
                        throw new Exception("Unknow parameter");
                }

                arrayIterator += elements[i].Size;
            }
            receivedSequence.Clear();
            OnFrameReady(new FrameReadyEventArgs(elements));
        }

        public void SaveToFile(string filename)
        {
            System.IO.StreamWriter sw = new System.IO.StreamWriter(filename);

            sw.WriteLine(StringParser.ByteArrayToStr(this.EndSequence));

            for (int i = 0; i < elements.Count; i++)
            {
                string tmp = elements[i].TypeOf.ToString() + ":" + elements[i].VarName;

                if (elements[i].TypeOf == _ElementType.BYTE_ARRAY)
                    tmp += "(" + elements[i].Size + ")";

                sw.WriteLine(tmp);
            }

            sw.Close();
        }

        public void LoadFromFile(string filename)
        {
            using (System.IO.StreamReader sr = new System.IO.StreamReader(filename))
            {
                elements.Clear();
                EndSequence = StringParser.StrToByteArray(sr.ReadLine());
                string tmp;

                while ((tmp = sr.ReadLine()) != null)
                {
                    string[] tArr = tmp.Split(':');
                    _ElementType elType = (_ElementType)Enum.Parse(typeof(_ElementType), tArr[0]);
                    int size = 0;

                    if (elType == _ElementType.BYTE_ARRAY)
                    {
                        var arrSize = System.Text.RegularExpressions.Regex.Match(tArr[1], @"\((\d+)\)");
                        size = Convert.ToInt32(arrSize.Groups[1].Value);
                        tArr[1] = tArr[1].Substring(0, tArr[1].Length - 2 - arrSize.Groups[1].Value.Length);
                    }

                    AddElement(elType, tArr[1], size);
                }
            }
        }
    }
}
