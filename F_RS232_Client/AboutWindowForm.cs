﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace F_RS232_Client
{
    public partial class AboutWindowForm : Form
    {
        public AboutWindowForm()
        {
            InitializeComponent();
            System.Reflection.AssemblyName asName = System.Reflection.Assembly.GetExecutingAssembly().GetName();
            nameLabel.Text = asName.Name;
            versionLabel.Text = "Version: " + asName.Version.ToString();
            copyrightLabel.Text = "Copyright  2012 " + ((DateTime.Now.Year == 2012) ? "" : " - " + DateTime.Now.Year.ToString()) + " Marcin Kolny";
            linkLabel1.LinkClicked += new LinkLabelLinkClickedEventHandler(LinkedLabelClicked);
        }

        private void LinkedLabelClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("http://f-rs232-client.3owl.com");
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
