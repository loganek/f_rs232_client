﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace F_RS232_Client
{
    public partial class ColorButton : UserControl
    {
        public ColorButton()
        {
            InitializeComponent();
        }

        public Color SelectedColor { set
        {
            this.BackColor = value;
        }
            get
            {
                return this.BackColor;
            }
        }

        private void ColorButton_Click(object sender, EventArgs e)
        {
            if (colorDialog.ShowDialog() == DialogResult.OK)
                this.SelectedColor = colorDialog.Color;
        }

        
    }
}
