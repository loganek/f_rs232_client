﻿namespace F_RS232_Client.Macros
{
    partial class MacroEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.topPanel = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.macroContentTextBox = new System.Windows.Forms.TextBox();
            this.macroDescriptionTextBox = new System.Windows.Forms.TextBox();
            this.addMacroButton = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.bottomPanel = new System.Windows.Forms.Panel();
            this.loadMacrosButton = new System.Windows.Forms.Button();
            this.saveMacrosButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.loadMacrosFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.saveMacrosFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.topPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // topPanel
            // 
            this.topPanel.Controls.Add(this.label2);
            this.topPanel.Controls.Add(this.label1);
            this.topPanel.Controls.Add(this.macroContentTextBox);
            this.topPanel.Controls.Add(this.macroDescriptionTextBox);
            this.topPanel.Controls.Add(this.addMacroButton);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(291, 56);
            this.topPanel.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(85, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Content:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Description:";
            // 
            // macroContentTextBox
            // 
            this.macroContentTextBox.Location = new System.Drawing.Point(88, 30);
            this.macroContentTextBox.Name = "macroContentTextBox";
            this.macroContentTextBox.Size = new System.Drawing.Size(107, 20);
            this.macroContentTextBox.TabIndex = 2;
            this.macroContentTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.macroDescriptionTextBox_KeyPress);
            // 
            // macroDescriptionTextBox
            // 
            this.macroDescriptionTextBox.Location = new System.Drawing.Point(9, 30);
            this.macroDescriptionTextBox.Name = "macroDescriptionTextBox";
            this.macroDescriptionTextBox.Size = new System.Drawing.Size(60, 20);
            this.macroDescriptionTextBox.TabIndex = 1;
            this.macroDescriptionTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.macroDescriptionTextBox_KeyPress);
            // 
            // addMacroButton
            // 
            this.addMacroButton.Location = new System.Drawing.Point(201, 4);
            this.addMacroButton.Name = "addMacroButton";
            this.addMacroButton.Size = new System.Drawing.Size(86, 46);
            this.addMacroButton.TabIndex = 0;
            this.addMacroButton.Text = "Add macro";
            this.addMacroButton.UseVisualStyleBackColor = true;
            this.addMacroButton.Click += new System.EventHandler(this.addMacroButton_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.AutoScroll = true;
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 56);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(291, 178);
            this.mainPanel.TabIndex = 0;
            // 
            // bottomPanel
            // 
            this.bottomPanel.Controls.Add(this.loadMacrosButton);
            this.bottomPanel.Controls.Add(this.saveMacrosButton);
            this.bottomPanel.Controls.Add(this.okButton);
            this.bottomPanel.Controls.Add(this.cancelButton);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.Location = new System.Drawing.Point(0, 234);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.Size = new System.Drawing.Size(291, 42);
            this.bottomPanel.TabIndex = 0;
            // 
            // loadMacrosButton
            // 
            this.loadMacrosButton.Location = new System.Drawing.Point(148, 3);
            this.loadMacrosButton.Name = "loadMacrosButton";
            this.loadMacrosButton.Size = new System.Drawing.Size(76, 34);
            this.loadMacrosButton.TabIndex = 1;
            this.loadMacrosButton.Text = "Load macros";
            this.loadMacrosButton.UseVisualStyleBackColor = true;
            this.loadMacrosButton.Click += new System.EventHandler(this.loadMacrosButton_Click);
            // 
            // saveMacrosButton
            // 
            this.saveMacrosButton.Location = new System.Drawing.Point(66, 3);
            this.saveMacrosButton.Name = "saveMacrosButton";
            this.saveMacrosButton.Size = new System.Drawing.Size(76, 34);
            this.saveMacrosButton.TabIndex = 0;
            this.saveMacrosButton.Text = "Save macros";
            this.saveMacrosButton.UseVisualStyleBackColor = true;
            this.saveMacrosButton.Click += new System.EventHandler(this.saveMacrosButton_Click);
            // 
            // okButton
            // 
            this.okButton.Location = new System.Drawing.Point(3, 3);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(57, 33);
            this.okButton.TabIndex = 5;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.okButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(230, 3);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(57, 34);
            this.cancelButton.TabIndex = 6;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // loadMacrosFileDialog
            // 
            this.loadMacrosFileDialog.FileName = "openFileDialog1";
            // 
            // MacroEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(291, 276);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.bottomPanel);
            this.Controls.Add(this.topPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MacroEditor";
            this.Text = "Edit macros";
            this.VisibleChanged += new System.EventHandler(this.MacroEditor_VisibleChanged);
            this.topPanel.ResumeLayout(false);
            this.topPanel.PerformLayout();
            this.bottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Panel bottomPanel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox macroContentTextBox;
        private System.Windows.Forms.TextBox macroDescriptionTextBox;
        private System.Windows.Forms.Button addMacroButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button saveMacrosButton;
        private System.Windows.Forms.Button loadMacrosButton;
        private System.Windows.Forms.OpenFileDialog loadMacrosFileDialog;
        private System.Windows.Forms.SaveFileDialog saveMacrosFileDialog;
    }
}