﻿namespace F_RS232_Client.Macros
{
    partial class MacroView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.editorPanel = new System.Windows.Forms.Panel();
            this.editMacrosButton = new System.Windows.Forms.Button();
            this.mainPanel = new System.Windows.Forms.Panel();
            this.hideFormButton = new System.Windows.Forms.Button();
            this.editorPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // editorPanel
            // 
            this.editorPanel.Controls.Add(this.editMacrosButton);
            this.editorPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.editorPanel.Location = new System.Drawing.Point(0, 0);
            this.editorPanel.Name = "editorPanel";
            this.editorPanel.Size = new System.Drawing.Size(292, 34);
            this.editorPanel.TabIndex = 0;
            // 
            // editMacrosButton
            // 
            this.editMacrosButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.editMacrosButton.Location = new System.Drawing.Point(0, 0);
            this.editMacrosButton.Name = "editMacrosButton";
            this.editMacrosButton.Size = new System.Drawing.Size(292, 34);
            this.editMacrosButton.TabIndex = 1;
            this.editMacrosButton.Text = "Edit macros";
            this.editMacrosButton.UseVisualStyleBackColor = true;
            this.editMacrosButton.Click += new System.EventHandler(this.editMacrosButton_Click);
            // 
            // mainPanel
            // 
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 34);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(292, 205);
            this.mainPanel.TabIndex = 0;
            // 
            // hideFormButton
            // 
            this.hideFormButton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.hideFormButton.Location = new System.Drawing.Point(0, 239);
            this.hideFormButton.Name = "hideFormButton";
            this.hideFormButton.Size = new System.Drawing.Size(292, 34);
            this.hideFormButton.TabIndex = 0;
            this.hideFormButton.Text = "Hide";
            this.hideFormButton.UseVisualStyleBackColor = true;
            this.hideFormButton.Click += new System.EventHandler(this.hideFormButton_Click);
            // 
            // MacroView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 273);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.hideFormButton);
            this.Controls.Add(this.editorPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MacroView";
            this.Text = "Macros";
            this.editorPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel editorPanel;
        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Button editMacrosButton;
        private System.Windows.Forms.Button hideFormButton;
    }
}