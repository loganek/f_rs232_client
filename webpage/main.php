﻿<?php
//error_reporting( E_ERROR );
function ShowFileInfo( $fileName, $version )
{
	$fp = fopen( $fileName, "r" ) or die('Couldn\'t open '.$filename);
	$bor = fgets( $fp, 1024 );
		
	echo '<h3>Version: '.$version.'</h3>';
	switch ( $bor )
	{
		case 1:
			echo '<span style="color:red">Alpha version!</span><br>';
			break;
		case 2:
			echo '<span style="color:brown">Beta version!</span><br>';
			break;
		case 3:
			echo '<span style="color:brown">Release version!</span><br>';
			break;
	}
	echo 'Filename: <b>'.fgets( $fp, 1024 );
	fgets( $fp, 1024 );
	echo '</b><br>Processor architecture: <b>'.fgets( $fp, 1024 );
	echo '</b><br>Date: <b>'.fgets( $fp, 1024 );
	
	$changelog = '';
	
	while ( ! feof( $fp ) ) 
		$changelog .= fgets( $fp, 1024 ).'<br>';
		
	echo '</b><br>Changelog: '.$changelog;
}

$showall = ( $_GET['showall'] == 1 ) ? true : false;

if ( $handle = opendir( 'logs/' ) ) 
{
	$files = array();
	while ($files[] = readdir($handle));
	arsort($files);

	foreach ($files as $entry) 
	{
		if ( $entry != "." && $entry != ".." &&  $entry != "")
		{	
			if ( $insideHandle = opendir( 'logs/'.$entry ) ) 
			{
				if ( $showall )
					echo '<br><fieldset><legend><h2>'.$entry.'</h2></legend>';
					
				$insideFiles = array();
				while ($insideFiles[] = readdir($insideHandle));
				arsort($insideFiles);
				
				foreach ($insideFiles as $insideEntry) 
				{
					if ( $insideEntry != "." && $insideEntry != ".." && $insideEntry !== false)
					{
						if ( $showall )
							ShowFileInfo( 'logs/'.$entry.'/'.$insideEntry, $insideEntry );
						else
						{
							$last = 'logs/'.$entry.'/'.$insideEntry;
							$version = $insideEntry;
							break;
						}
					}
				}
				closedir( $insideHandle );
				
				if ( !$showall )
					break;
				
				if ( $showall )
					echo '</fieldset>';
			}
		}
	}
    closedir( $handle );
	
	if ( !$showall )
	{
		echo '<h2>Last version</h2>';
		ShowFileInfo($last, $version);
	}
}
?>
	