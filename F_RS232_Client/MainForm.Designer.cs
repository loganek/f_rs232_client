﻿namespace F_RS232_Client
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.stopBitsComboBox = new System.Windows.Forms.ComboBox();
            this.connectButton = new System.Windows.Forms.Button();
            this.stopBitsLabel = new System.Windows.Forms.Label();
            this.parityComboBox = new System.Windows.Forms.ComboBox();
            this.parityLabel = new System.Windows.Forms.Label();
            this.dataBitsComboBox = new System.Windows.Forms.ComboBox();
            this.dataBitsLabel = new System.Windows.Forms.Label();
            this.baudComboBox = new System.Windows.Forms.ComboBox();
            this.baudLabel = new System.Windows.Forms.Label();
            this.rescanButton = new System.Windows.Forms.Button();
            this.portyComboBox = new System.Windows.Forms.ComboBox();
            this.mainSplitContainer = new System.Windows.Forms.SplitContainer();
            this.sendingGroupBox = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.newLineSendCheckBox = new System.Windows.Forms.CheckBox();
            this.delTextCheckBox = new System.Windows.Forms.CheckBox();
            this.plusLFCheckBox = new System.Windows.Forms.CheckBox();
            this.plusCRCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.circularSendingCheckBox = new System.Windows.Forms.CheckBox();
            this.circularTimeoutTextBox = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.asciiSendCharsRadioButton = new System.Windows.Forms.RadioButton();
            this.hexSendCharsRadioButton = new System.Windows.Forms.RadioButton();
            this.clearSentButton = new System.Windows.Forms.Button();
            this.macroPluginButton = new System.Windows.Forms.Button();
            this.sendTextBox = new System.Windows.Forms.TextBox();
            this.sendButton = new System.Windows.Forms.Button();
            this.sendingRichTextBox = new System.Windows.Forms.RichTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.clearFrameValuesButton = new System.Windows.Forms.Button();
            this.graphButton = new System.Windows.Forms.Button();
            this.showFrameDataButton = new System.Windows.Forms.Button();
            this.receiveSplitContainer = new System.Windows.Forms.SplitContainer();
            this.receiveRichTextBox = new System.Windows.Forms.RichTextBox();
            this.receivedFrameListView = new System.Windows.Forms.ListView();
            this.frameEditorButton = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.hexReceiveDisplayModeRadioButton = new System.Windows.Forms.RadioButton();
            this.asciiReceiveDisplayModeRadioButton = new System.Windows.Forms.RadioButton();
            this.clearReceivedButton = new System.Windows.Forms.Button();
            this.circularSendingTimer = new System.Windows.Forms.Timer(this.components);
            this.mainMenuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.optionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.recRefreshControlsTimer = new System.Windows.Forms.Timer(this.components);
            this.sendRefreshControlsTimer = new System.Windows.Forms.Timer(this.components);
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).BeginInit();
            this.mainSplitContainer.Panel1.SuspendLayout();
            this.mainSplitContainer.Panel2.SuspendLayout();
            this.mainSplitContainer.SuspendLayout();
            this.sendingGroupBox.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.receiveSplitContainer)).BeginInit();
            this.receiveSplitContainer.Panel1.SuspendLayout();
            this.receiveSplitContainer.Panel2.SuspendLayout();
            this.receiveSplitContainer.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.mainMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.stopBitsComboBox);
            this.groupBox1.Controls.Add(this.connectButton);
            this.groupBox1.Controls.Add(this.stopBitsLabel);
            this.groupBox1.Controls.Add(this.parityComboBox);
            this.groupBox1.Controls.Add(this.parityLabel);
            this.groupBox1.Controls.Add(this.dataBitsComboBox);
            this.groupBox1.Controls.Add(this.dataBitsLabel);
            this.groupBox1.Controls.Add(this.baudComboBox);
            this.groupBox1.Controls.Add(this.baudLabel);
            this.groupBox1.Controls.Add(this.rescanButton);
            this.groupBox1.Controls.Add(this.portyComboBox);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(158, 167);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Connection params";
            // 
            // stopBitsComboBox
            // 
            this.stopBitsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.stopBitsComboBox.FormattingEnabled = true;
            this.stopBitsComboBox.Items.AddRange(new object[] {
            "1",
            "1,5",
            "2"});
            this.stopBitsComboBox.Location = new System.Drawing.Point(76, 86);
            this.stopBitsComboBox.Name = "stopBitsComboBox";
            this.stopBitsComboBox.Size = new System.Drawing.Size(76, 21);
            this.stopBitsComboBox.TabIndex = 9;
            // 
            // connectButton
            // 
            this.connectButton.Location = new System.Drawing.Point(6, 138);
            this.connectButton.Name = "connectButton";
            this.connectButton.Size = new System.Drawing.Size(146, 23);
            this.connectButton.TabIndex = 10;
            this.connectButton.Text = "Connect";
            this.connectButton.UseVisualStyleBackColor = true;
            this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // stopBitsLabel
            // 
            this.stopBitsLabel.AutoSize = true;
            this.stopBitsLabel.Location = new System.Drawing.Point(73, 69);
            this.stopBitsLabel.Name = "stopBitsLabel";
            this.stopBitsLabel.Size = new System.Drawing.Size(48, 13);
            this.stopBitsLabel.TabIndex = 8;
            this.stopBitsLabel.Text = "Stop bits";
            // 
            // parityComboBox
            // 
            this.parityComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.parityComboBox.FormattingEnabled = true;
            this.parityComboBox.Items.AddRange(new object[] {
            "none",
            "odd",
            "even",
            "mark",
            "space"});
            this.parityComboBox.Location = new System.Drawing.Point(76, 113);
            this.parityComboBox.Name = "parityComboBox";
            this.parityComboBox.Size = new System.Drawing.Size(76, 21);
            this.parityComboBox.TabIndex = 7;
            // 
            // parityLabel
            // 
            this.parityLabel.AutoSize = true;
            this.parityLabel.Location = new System.Drawing.Point(3, 113);
            this.parityLabel.Name = "parityLabel";
            this.parityLabel.Size = new System.Drawing.Size(66, 13);
            this.parityLabel.TabIndex = 6;
            this.parityLabel.Text = "Parity-check";
            // 
            // dataBitsComboBox
            // 
            this.dataBitsComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.dataBitsComboBox.FormattingEnabled = true;
            this.dataBitsComboBox.Items.AddRange(new object[] {
            "5",
            "6",
            "7",
            "8"});
            this.dataBitsComboBox.Location = new System.Drawing.Point(6, 85);
            this.dataBitsComboBox.Name = "dataBitsComboBox";
            this.dataBitsComboBox.Size = new System.Drawing.Size(64, 21);
            this.dataBitsComboBox.TabIndex = 5;
            // 
            // dataBitsLabel
            // 
            this.dataBitsLabel.AutoSize = true;
            this.dataBitsLabel.Location = new System.Drawing.Point(3, 69);
            this.dataBitsLabel.Name = "dataBitsLabel";
            this.dataBitsLabel.Size = new System.Drawing.Size(49, 13);
            this.dataBitsLabel.TabIndex = 4;
            this.dataBitsLabel.Text = "Data bits";
            // 
            // baudComboBox
            // 
            this.baudComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.baudComboBox.FormattingEnabled = true;
            this.baudComboBox.Items.AddRange(new object[] {
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "14400",
            "19200",
            "28800",
            "38400",
            "56000",
            "57600",
            "115200",
            "128000",
            "256000"});
            this.baudComboBox.Location = new System.Drawing.Point(76, 43);
            this.baudComboBox.Name = "baudComboBox";
            this.baudComboBox.Size = new System.Drawing.Size(76, 21);
            this.baudComboBox.TabIndex = 3;
            // 
            // baudLabel
            // 
            this.baudLabel.AutoSize = true;
            this.baudLabel.Location = new System.Drawing.Point(3, 46);
            this.baudLabel.Name = "baudLabel";
            this.baudLabel.Size = new System.Drawing.Size(53, 13);
            this.baudLabel.TabIndex = 2;
            this.baudLabel.Text = "Baud rate";
            // 
            // rescanButton
            // 
            this.rescanButton.Location = new System.Drawing.Point(6, 19);
            this.rescanButton.Name = "rescanButton";
            this.rescanButton.Size = new System.Drawing.Size(61, 21);
            this.rescanButton.TabIndex = 1;
            this.rescanButton.Text = "Rescan";
            this.rescanButton.UseVisualStyleBackColor = true;
            this.rescanButton.Click += new System.EventHandler(this.rescanButton_Click);
            // 
            // portyComboBox
            // 
            this.portyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.portyComboBox.FormattingEnabled = true;
            this.portyComboBox.Location = new System.Drawing.Point(76, 19);
            this.portyComboBox.Name = "portyComboBox";
            this.portyComboBox.Size = new System.Drawing.Size(76, 21);
            this.portyComboBox.TabIndex = 0;
            // 
            // mainSplitContainer
            // 
            this.mainSplitContainer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this.mainSplitContainer.Location = new System.Drawing.Point(0, 24);
            this.mainSplitContainer.Name = "mainSplitContainer";
            this.mainSplitContainer.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // mainSplitContainer.Panel1
            // 
            this.mainSplitContainer.Panel1.Controls.Add(this.sendingGroupBox);
            this.mainSplitContainer.Panel1.Controls.Add(this.groupBox1);
            // 
            // mainSplitContainer.Panel2
            // 
            this.mainSplitContainer.Panel2.Controls.Add(this.groupBox4);
            this.mainSplitContainer.Size = new System.Drawing.Size(873, 316);
            this.mainSplitContainer.SplitterDistance = 177;
            this.mainSplitContainer.TabIndex = 11;
            // 
            // sendingGroupBox
            // 
            this.sendingGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sendingGroupBox.Controls.Add(this.groupBox6);
            this.sendingGroupBox.Controls.Add(this.plusLFCheckBox);
            this.sendingGroupBox.Controls.Add(this.plusCRCheckBox);
            this.sendingGroupBox.Controls.Add(this.groupBox3);
            this.sendingGroupBox.Controls.Add(this.groupBox2);
            this.sendingGroupBox.Controls.Add(this.clearSentButton);
            this.sendingGroupBox.Controls.Add(this.macroPluginButton);
            this.sendingGroupBox.Controls.Add(this.sendTextBox);
            this.sendingGroupBox.Controls.Add(this.sendButton);
            this.sendingGroupBox.Controls.Add(this.sendingRichTextBox);
            this.sendingGroupBox.Location = new System.Drawing.Point(167, 3);
            this.sendingGroupBox.Name = "sendingGroupBox";
            this.sendingGroupBox.Size = new System.Drawing.Size(703, 172);
            this.sendingGroupBox.TabIndex = 3;
            this.sendingGroupBox.TabStop = false;
            this.sendingGroupBox.Text = "Sending";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.newLineSendCheckBox);
            this.groupBox6.Controls.Add(this.delTextCheckBox);
            this.groupBox6.Location = new System.Drawing.Point(606, 62);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(97, 103);
            this.groupBox6.TabIndex = 14;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Display options";
            // 
            // newLineSendCheckBox
            // 
            this.newLineSendCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.newLineSendCheckBox.AutoSize = true;
            this.newLineSendCheckBox.Location = new System.Drawing.Point(6, 19);
            this.newLineSendCheckBox.Name = "newLineSendCheckBox";
            this.newLineSendCheckBox.Size = new System.Drawing.Size(87, 30);
            this.newLineSendCheckBox.TabIndex = 4;
            this.newLineSendCheckBox.Text = "New line\r\nafter sending";
            this.newLineSendCheckBox.UseVisualStyleBackColor = true;
            // 
            // delTextCheckBox
            // 
            this.delTextCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.delTextCheckBox.AutoSize = true;
            this.delTextCheckBox.Checked = true;
            this.delTextCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.delTextCheckBox.Location = new System.Drawing.Point(5, 60);
            this.delTextCheckBox.Name = "delTextCheckBox";
            this.delTextCheckBox.Size = new System.Drawing.Size(87, 30);
            this.delTextCheckBox.TabIndex = 3;
            this.delTextCheckBox.Text = "Delete text\r\nafter sending";
            this.delTextCheckBox.UseVisualStyleBackColor = true;
            // 
            // plusLFCheckBox
            // 
            this.plusLFCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plusLFCheckBox.AutoSize = true;
            this.plusLFCheckBox.Location = new System.Drawing.Point(510, 145);
            this.plusLFCheckBox.Name = "plusLFCheckBox";
            this.plusLFCheckBox.Size = new System.Drawing.Size(44, 17);
            this.plusLFCheckBox.TabIndex = 13;
            this.plusLFCheckBox.Text = "+LF";
            this.plusLFCheckBox.UseVisualStyleBackColor = true;
            // 
            // plusCRCheckBox
            // 
            this.plusCRCheckBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.plusCRCheckBox.AutoSize = true;
            this.plusCRCheckBox.Location = new System.Drawing.Point(457, 146);
            this.plusCRCheckBox.Name = "plusCRCheckBox";
            this.plusCRCheckBox.Size = new System.Drawing.Size(47, 17);
            this.plusCRCheckBox.TabIndex = 12;
            this.plusCRCheckBox.Text = "+CR";
            this.plusCRCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.circularSendingCheckBox);
            this.groupBox3.Controls.Add(this.circularTimeoutTextBox);
            this.groupBox3.Location = new System.Drawing.Point(492, 62);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(113, 44);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Circular sending";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(53, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "ms";
            // 
            // circularSendingCheckBox
            // 
            this.circularSendingCheckBox.AutoSize = true;
            this.circularSendingCheckBox.Enabled = false;
            this.circularSendingCheckBox.Location = new System.Drawing.Point(79, 18);
            this.circularSendingCheckBox.Name = "circularSendingCheckBox";
            this.circularSendingCheckBox.Size = new System.Drawing.Size(15, 14);
            this.circularSendingCheckBox.TabIndex = 1;
            this.circularSendingCheckBox.UseVisualStyleBackColor = true;
            this.circularSendingCheckBox.CheckedChanged += new System.EventHandler(this.circularSendingCheckBox_CheckedChanged);
            // 
            // circularTimeoutTextBox
            // 
            this.circularTimeoutTextBox.Location = new System.Drawing.Point(6, 16);
            this.circularTimeoutTextBox.Name = "circularTimeoutTextBox";
            this.circularTimeoutTextBox.Size = new System.Drawing.Size(44, 20);
            this.circularTimeoutTextBox.TabIndex = 0;
            this.circularTimeoutTextBox.Text = "1000";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.asciiSendCharsRadioButton);
            this.groupBox2.Controls.Add(this.hexSendCharsRadioButton);
            this.groupBox2.Location = new System.Drawing.Point(492, 9);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(113, 47);
            this.groupBox2.TabIndex = 11;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Display mode";
            // 
            // asciiSendCharsRadioButton
            // 
            this.asciiSendCharsRadioButton.AutoSize = true;
            this.asciiSendCharsRadioButton.Checked = true;
            this.asciiSendCharsRadioButton.Location = new System.Drawing.Point(6, 19);
            this.asciiSendCharsRadioButton.Name = "asciiSendCharsRadioButton";
            this.asciiSendCharsRadioButton.Size = new System.Drawing.Size(52, 17);
            this.asciiSendCharsRadioButton.TabIndex = 2;
            this.asciiSendCharsRadioButton.TabStop = true;
            this.asciiSendCharsRadioButton.Text = "ASCII";
            this.asciiSendCharsRadioButton.UseVisualStyleBackColor = true;
            // 
            // hexSendCharsRadioButton
            // 
            this.hexSendCharsRadioButton.AutoSize = true;
            this.hexSendCharsRadioButton.Location = new System.Drawing.Point(64, 19);
            this.hexSendCharsRadioButton.Name = "hexSendCharsRadioButton";
            this.hexSendCharsRadioButton.Size = new System.Drawing.Size(44, 17);
            this.hexSendCharsRadioButton.TabIndex = 0;
            this.hexSendCharsRadioButton.Text = "Hex";
            this.hexSendCharsRadioButton.UseVisualStyleBackColor = true;
            // 
            // clearSentButton
            // 
            this.clearSentButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.clearSentButton.Location = new System.Drawing.Point(607, 13);
            this.clearSentButton.Name = "clearSentButton";
            this.clearSentButton.Size = new System.Drawing.Size(91, 43);
            this.clearSentButton.TabIndex = 7;
            this.clearSentButton.Text = "Clear console";
            this.clearSentButton.UseVisualStyleBackColor = true;
            this.clearSentButton.Click += new System.EventHandler(this.clearSentButton_Click);
            // 
            // macroPluginButton
            // 
            this.macroPluginButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.macroPluginButton.Location = new System.Drawing.Point(492, 108);
            this.macroPluginButton.Name = "macroPluginButton";
            this.macroPluginButton.Size = new System.Drawing.Size(113, 26);
            this.macroPluginButton.TabIndex = 5;
            this.macroPluginButton.Text = "Macros...";
            this.macroPluginButton.UseVisualStyleBackColor = true;
            this.macroPluginButton.Click += new System.EventHandler(this.macroPluginButton_Click);
            // 
            // sendTextBox
            // 
            this.sendTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sendTextBox.Location = new System.Drawing.Point(6, 143);
            this.sendTextBox.Name = "sendTextBox";
            this.sendTextBox.Size = new System.Drawing.Size(445, 20);
            this.sendTextBox.TabIndex = 2;
            this.sendTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.sendTextBox_KeyPress);
            // 
            // sendButton
            // 
            this.sendButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.sendButton.Enabled = false;
            this.sendButton.Location = new System.Drawing.Point(555, 139);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(45, 26);
            this.sendButton.TabIndex = 1;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
            // 
            // sendingRichTextBox
            // 
            this.sendingRichTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.sendingRichTextBox.Location = new System.Drawing.Point(6, 16);
            this.sendingRichTextBox.Name = "sendingRichTextBox";
            this.sendingRichTextBox.ReadOnly = true;
            this.sendingRichTextBox.Size = new System.Drawing.Size(480, 123);
            this.sendingRichTextBox.TabIndex = 0;
            this.sendingRichTextBox.Text = "";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.clearFrameValuesButton);
            this.groupBox4.Controls.Add(this.graphButton);
            this.groupBox4.Controls.Add(this.showFrameDataButton);
            this.groupBox4.Controls.Add(this.receiveSplitContainer);
            this.groupBox4.Controls.Add(this.frameEditorButton);
            this.groupBox4.Controls.Add(this.groupBox5);
            this.groupBox4.Controls.Add(this.clearReceivedButton);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(0, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(873, 135);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Receiving";
            // 
            // clearFrameValuesButton
            // 
            this.clearFrameValuesButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clearFrameValuesButton.Location = new System.Drawing.Point(403, 90);
            this.clearFrameValuesButton.Name = "clearFrameValuesButton";
            this.clearFrameValuesButton.Size = new System.Drawing.Size(68, 38);
            this.clearFrameValuesButton.TabIndex = 16;
            this.clearFrameValuesButton.Text = "Clear frame values";
            this.clearFrameValuesButton.UseVisualStyleBackColor = true;
            this.clearFrameValuesButton.Click += new System.EventHandler(this.clearFrameValuesButton_Click);
            // 
            // graphButton
            // 
            this.graphButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.graphButton.Location = new System.Drawing.Point(328, 90);
            this.graphButton.Name = "graphButton";
            this.graphButton.Size = new System.Drawing.Size(69, 38);
            this.graphButton.TabIndex = 15;
            this.graphButton.Text = "Graph";
            this.graphButton.UseVisualStyleBackColor = true;
            this.graphButton.Click += new System.EventHandler(this.graphButton_Click);
            // 
            // showFrameDataButton
            // 
            this.showFrameDataButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.showFrameDataButton.Location = new System.Drawing.Point(253, 90);
            this.showFrameDataButton.Name = "showFrameDataButton";
            this.showFrameDataButton.Size = new System.Drawing.Size(69, 39);
            this.showFrameDataButton.TabIndex = 14;
            this.showFrameDataButton.Text = "Show/Hide frame data";
            this.showFrameDataButton.UseVisualStyleBackColor = true;
            this.showFrameDataButton.Click += new System.EventHandler(this.showFrameDataButton_Click);
            // 
            // receiveSplitContainer
            // 
            this.receiveSplitContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.receiveSplitContainer.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.receiveSplitContainer.Location = new System.Drawing.Point(9, 19);
            this.receiveSplitContainer.Name = "receiveSplitContainer";
            // 
            // receiveSplitContainer.Panel1
            // 
            this.receiveSplitContainer.Panel1.Controls.Add(this.receiveRichTextBox);
            // 
            // receiveSplitContainer.Panel2
            // 
            this.receiveSplitContainer.Panel2.Controls.Add(this.receivedFrameListView);
            this.receiveSplitContainer.Panel2Collapsed = true;
            this.receiveSplitContainer.Size = new System.Drawing.Size(852, 65);
            this.receiveSplitContainer.SplitterDistance = 659;
            this.receiveSplitContainer.TabIndex = 13;
            // 
            // receiveRichTextBox
            // 
            this.receiveRichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.receiveRichTextBox.Location = new System.Drawing.Point(0, 0);
            this.receiveRichTextBox.Name = "receiveRichTextBox";
            this.receiveRichTextBox.ReadOnly = true;
            this.receiveRichTextBox.Size = new System.Drawing.Size(852, 65);
            this.receiveRichTextBox.TabIndex = 0;
            this.receiveRichTextBox.Text = "";
            // 
            // receivedFrameListView
            // 
            this.receivedFrameListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.receivedFrameListView.Location = new System.Drawing.Point(0, 0);
            this.receivedFrameListView.Name = "receivedFrameListView";
            this.receivedFrameListView.Size = new System.Drawing.Size(96, 100);
            this.receivedFrameListView.TabIndex = 0;
            this.receivedFrameListView.UseCompatibleStateImageBehavior = false;
            this.receivedFrameListView.View = System.Windows.Forms.View.Details;
            this.receivedFrameListView.Resize += new System.EventHandler(this.receivedFrameListView_Resize);
            // 
            // frameEditorButton
            // 
            this.frameEditorButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.frameEditorButton.Location = new System.Drawing.Point(189, 90);
            this.frameEditorButton.Name = "frameEditorButton";
            this.frameEditorButton.Size = new System.Drawing.Size(58, 38);
            this.frameEditorButton.TabIndex = 12;
            this.frameEditorButton.Text = "Frame editor";
            this.frameEditorButton.UseVisualStyleBackColor = true;
            this.frameEditorButton.Click += new System.EventHandler(this.frameEditorButton_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox5.Controls.Add(this.hexReceiveDisplayModeRadioButton);
            this.groupBox5.Controls.Add(this.asciiReceiveDisplayModeRadioButton);
            this.groupBox5.Location = new System.Drawing.Point(79, 90);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(104, 39);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Display mode";
            // 
            // hexReceiveDisplayModeRadioButton
            // 
            this.hexReceiveDisplayModeRadioButton.AutoSize = true;
            this.hexReceiveDisplayModeRadioButton.Location = new System.Drawing.Point(54, 16);
            this.hexReceiveDisplayModeRadioButton.Name = "hexReceiveDisplayModeRadioButton";
            this.hexReceiveDisplayModeRadioButton.Size = new System.Drawing.Size(44, 17);
            this.hexReceiveDisplayModeRadioButton.TabIndex = 1;
            this.hexReceiveDisplayModeRadioButton.Text = "Hex";
            this.hexReceiveDisplayModeRadioButton.UseVisualStyleBackColor = true;
            // 
            // asciiReceiveDisplayModeRadioButton
            // 
            this.asciiReceiveDisplayModeRadioButton.AutoSize = true;
            this.asciiReceiveDisplayModeRadioButton.Checked = true;
            this.asciiReceiveDisplayModeRadioButton.Location = new System.Drawing.Point(3, 16);
            this.asciiReceiveDisplayModeRadioButton.Name = "asciiReceiveDisplayModeRadioButton";
            this.asciiReceiveDisplayModeRadioButton.Size = new System.Drawing.Size(52, 17);
            this.asciiReceiveDisplayModeRadioButton.TabIndex = 0;
            this.asciiReceiveDisplayModeRadioButton.TabStop = true;
            this.asciiReceiveDisplayModeRadioButton.Text = "ASCII";
            this.asciiReceiveDisplayModeRadioButton.UseVisualStyleBackColor = true;
            // 
            // clearReceivedButton
            // 
            this.clearReceivedButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clearReceivedButton.Location = new System.Drawing.Point(12, 90);
            this.clearReceivedButton.Name = "clearReceivedButton";
            this.clearReceivedButton.Size = new System.Drawing.Size(61, 38);
            this.clearReceivedButton.TabIndex = 1;
            this.clearReceivedButton.Text = "Clear console";
            this.clearReceivedButton.UseVisualStyleBackColor = true;
            this.clearReceivedButton.Click += new System.EventHandler(this.clearReceivedButton_Click);
            // 
            // circularSendingTimer
            // 
            this.circularSendingTimer.Tick += new System.EventHandler(this.circularSendingTimer_Tick);
            // 
            // mainMenuStrip
            // 
            this.mainMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.mainMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.mainMenuStrip.Name = "mainMenuStrip";
            this.mainMenuStrip.Size = new System.Drawing.Size(873, 24);
            this.mainMenuStrip.TabIndex = 5;
            this.mainMenuStrip.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.openToolStripMenuItem,
            this.toolStripSeparator,
            this.toolStripMenuItem2,
            this.saveToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem1.Image")));
            this.toolStripMenuItem1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.toolStripMenuItem1.Size = new System.Drawing.Size(198, 22);
            this.toolStripMenuItem1.Text = "&Load macros";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.openToolStripMenuItem.Text = "&Load frame";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
            // 
            // toolStripSeparator
            // 
            this.toolStripSeparator.Name = "toolStripSeparator";
            this.toolStripSeparator.Size = new System.Drawing.Size(195, 6);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripMenuItem2.Image")));
            this.toolStripMenuItem2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.toolStripMenuItem2.Size = new System.Drawing.Size(198, 22);
            this.toolStripMenuItem2.Text = "&Save macros";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.saveToolStripMenuItem.Text = "&Save frame";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(195, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(198, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.optionsToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.toolsToolStripMenuItem.Text = "&Tools";
            // 
            // optionsToolStripMenuItem
            // 
            this.optionsToolStripMenuItem.Name = "optionsToolStripMenuItem";
            this.optionsToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.optionsToolStripMenuItem.Text = "&Options";
            this.optionsToolStripMenuItem.Click += new System.EventHandler(this.optionsToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.ShortcutKeys = System.Windows.Forms.Keys.F1;
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(134, 22);
            this.aboutToolStripMenuItem.Text = "&About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // recRefreshControlsTimer
            // 
            this.recRefreshControlsTimer.Interval = 1000;
            this.recRefreshControlsTimer.Tick += new System.EventHandler(this.refreshControlsTimer_Tick);
            // 
            // sendRefreshControlsTimer
            // 
            this.sendRefreshControlsTimer.Interval = 1000;
            this.sendRefreshControlsTimer.Tick += new System.EventHandler(this.sendRefreshControlsTimer_Tick);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog1";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(873, 340);
            this.Controls.Add(this.mainSplitContainer);
            this.Controls.Add(this.mainMenuStrip);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.mainMenuStrip;
            this.Name = "MainForm";
            this.Text = "F_RS232_Client";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.mainSplitContainer.Panel1.ResumeLayout(false);
            this.mainSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.mainSplitContainer)).EndInit();
            this.mainSplitContainer.ResumeLayout(false);
            this.sendingGroupBox.ResumeLayout(false);
            this.sendingGroupBox.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.receiveSplitContainer.Panel1.ResumeLayout(false);
            this.receiveSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.receiveSplitContainer)).EndInit();
            this.receiveSplitContainer.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.mainMenuStrip.ResumeLayout(false);
            this.mainMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.ComboBox stopBitsComboBox;
        private System.Windows.Forms.Label stopBitsLabel;
        private System.Windows.Forms.ComboBox parityComboBox;
        private System.Windows.Forms.Label parityLabel;
        private System.Windows.Forms.ComboBox dataBitsComboBox;
        private System.Windows.Forms.Label dataBitsLabel;
        private System.Windows.Forms.ComboBox baudComboBox;
        private System.Windows.Forms.Label baudLabel;
        private System.Windows.Forms.Button rescanButton;
        private System.Windows.Forms.ComboBox portyComboBox;
        private System.Windows.Forms.SplitContainer mainSplitContainer;
        private System.Windows.Forms.GroupBox sendingGroupBox;
        private System.Windows.Forms.Button clearSentButton;
        private System.Windows.Forms.Button macroPluginButton;
        private System.Windows.Forms.TextBox sendTextBox;
        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.RichTextBox sendingRichTextBox;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton hexSendCharsRadioButton;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox circularSendingCheckBox;
        private System.Windows.Forms.TextBox circularTimeoutTextBox;
        private System.Windows.Forms.Timer circularSendingTimer;
        private System.Windows.Forms.RadioButton asciiSendCharsRadioButton;
        private System.Windows.Forms.CheckBox plusLFCheckBox;
        private System.Windows.Forms.CheckBox plusCRCheckBox;
        private System.Windows.Forms.RichTextBox receiveRichTextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button clearReceivedButton;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.RadioButton hexReceiveDisplayModeRadioButton;
        private System.Windows.Forms.RadioButton asciiReceiveDisplayModeRadioButton;
        private System.Windows.Forms.Button frameEditorButton;
        private System.Windows.Forms.SplitContainer receiveSplitContainer;
        private System.Windows.Forms.Button showFrameDataButton;
        private System.Windows.Forms.ListView receivedFrameListView;
        private System.Windows.Forms.Button graphButton;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox newLineSendCheckBox;
        private System.Windows.Forms.CheckBox delTextCheckBox;
        private System.Windows.Forms.MenuStrip mainMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem optionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Timer recRefreshControlsTimer;
        private System.Windows.Forms.Timer sendRefreshControlsTimer;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.Button clearFrameValuesButton;

    }
}

