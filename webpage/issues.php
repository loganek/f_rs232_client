﻿<?php

$priority_array = array( 'low', 'medium', 'high');

$bug = 0;

if ( isset($_POST['add_issue'] ) )
{	
	$nick = htmlspecialchars($_POST['nick']);
	
	if ( strlen($nick) < 1 )
	{
		echo 'Enter correct nick!<br>';
		$bug = 1;
	}
	
	$email = htmlspecialchars($_POST['email']);
	$priority = intval($_POST['priority']);
	
	if($priority < 0 || $priority >2 )
		$priority = 2;
	
	$title = htmlspecialchars($_POST['title']);
	
	if ( strlen($title) < 3 )
	{
		echo 'Enter correct title!<br>';
		$bug = 1;
	}
	
	$description = htmlspecialchars($_POST['description']);
	
	if ( strlen($nick) < 5 )
	{
		echo 'Enter correct description!<br>';
		$bug = 1;
	}
	
	if ( $bug == 0 )
	{
		if ( mysql_num_rows( mysql_query( 'select id from issues where title = "'.$title.'" and description = "'.$description.'" and nick = "'.$nick.'"' ) ) == 0 )
		{
			mysql_query('insert into issues values(NULL, "'.$nick.'", "'.$email.'", "'.$title.'", "'.$description.'", NOW(), '.$priority.', "")');
			echo '<br><b>Thanks for add new issue! I will try to solve it as soon as possible.</b><br>';
		}
	}
}

if ( $_GET['add_issue'] == '1' || $bug == 1 )
{
?>
<form action='/issues' method='post'>
	<table border=0>
	<tr><td>Your nick: </td><td><input name=nick></td></tr>
	<tr><td>Your email(invisible for users, only for me:) ): </td><td><input name=email></td></tr>
	<tr><td>Priority: </td><td><select name=priority>
	<option value=0>low</option>
	<option value=1>medium</option>
	<option value=2>high</option>
	</select></td></tr>
	<tr><td>Issue title: </td><td><input name=title></td></tr>
	<tr><td>Issue description: </td><td><textarea name=description></textarea></td></tr>
	<tr><td></td><td><input type=submit value='Add issue' name='add_issue'></td></tr>
</form>
<?php
}

function ShowSolvedInfo($solved)
{
	global $priority_array;

	if ( strlen($solved) > 6 )
		return '<span style="color:green">solved in version <b>'.$solved.'</b></span>';
	else
		return '<span style="color:red">not solved yet</span>';	
}

function PrintSingleIssueFull($record)
{
	global $priority_array;
	
	echo '<h3>Issue #'.$record['id'].': '.$record['title'].'</h3>';
	echo '<i>Reported by: '.$record['nick'].' on '.$record['date'].'</i><br>';
	echo 'Priority: '.$priority_array[$record['priority']].'<br>';
	echo ShowSolvedInfo($record['solved']);
	echo '<br>Description: <br>';
	echo $record['description'];
}

function PrintSingleIssueRow($record, $color)
{
	global $priority_array;

	echo '<tr onmouseover="style.backgroundColor=\'#FFCCCC\'" onmouseout="style.backgroundColor=\''.$color.'\'" style="background-color:'.$color.'"><td><a href="/issues/'.$record['id'].'">#'.$record['id'].'</a></td><td>'.$priority_array[$record['priority']].'</td><td>'.$record['date'].'</td><td>'.$record['nick'].'</td><td>'.$record['title'].'</td><td>'.ShowSolvedInfo($record['solved']).'</td>';
	
	if ( $_SESSION['logged'] == 723 )
	{
		echo '<td>';
		if ( strlen($record['solved']) > 6 ) 
		{
			echo'<a href= "/issues/unsolve/'.$record['id'].'">Unsolve</a>';
		}
		else
		{
			echo '<form method="post" action="/issues/solve"><input type=hidden name=rec_id value="'.$record['id'].'">Solve</a> <input name="version" size="8"> <input type=submit value="OK"></form>';
		}
		echo '</td>';
		echo '<td><a href="/issues/remove/'.$record['id'].'">Remove</a></td>';
	}
	
	echo '</tr>';
}

if ( isset( $_POST['rec_id'] ) )
{
	mysql_query( 'update issues set solved="'.htmlspecialchars($_POST['version']).'" where id='.intval($_POST['rec_id']));
}
else if ( isset( $_GET['unsolve'] ))
{
	mysql_query( 'update issues set solved="" where id='.intval($_GET['unsolve']));
}

if ( isset( $_GET['remove'] ) )
{
	mysql_query( 'delete from issues where id = '.intval($_GET['remove']));
}

if ( isset( $_GET['issue_id'] ) )
{
	$issue_id = intval( $_GET['issue_id'] );
	$query = mysql_query( 'select * from issues where id = '.$issue_id );
	
	if ( mysql_num_rows( $query ) == 0 )
		echo 'cannot find issue id #'.$issue_id.'<br>';
	else
	{
		PrintSingleIssueFull( mysql_fetch_array( $query ) );
	}
}
else
{
	switch ( $_GET['sortby'] )
	{
		case 'id':
			$w = 'id';
			break;
		case 'priority':
			$w = 'priority';
			break;
		case 'date':
			$w = 'date';
			break;
		case 'title':
			$w = 'title';
			break;
		case 'reportedby':
			$w = 'nick';
			break;
		case 'solved':
			$w = 'solved';
			break;
		default:
			$w = '';
	}
	
	if ( $w != '' )
		$w = 'order by '.$w;
	else 
		$w = 'order by id desc';
		
	$query = mysql_query( 'select * from issues '.$w );
	echo '<table>';
	echo '<tr><td><a href="/issues/sortby/id">Issue ID</a></td><td><a href="/issues/sortby/priority">Priority</a></td><td><a href="/issues/sortby/date">Date</a></td><td><a href="/issues/sortby/reportedby">Reported by</a></td><td><a href="/issues/sortby/title">Title</a></td><td><a href="/issues/sortby/solved">Solved</a></td>';
	
	if ( $_SESSION['logged'] == 723 )
	{
		echo '<td>Solve / unsolve</td><td>Remove</td>';
	}
	
	echo '</tr>';
	$i = 0;
	
	while ( $record = mysql_fetch_array( $query ) )
	{
		PrintSingleIssueRow( $record,  ( $i++ % 2 ) ? 'CCCCFF' : '#CCFFCC' );
	}
	
	echo '</table>';
}
?>
	</body>
</html>