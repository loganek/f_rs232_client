﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using System.IO;
using System.Net;
using System.Diagnostics;

namespace WebPublisher
{
    public partial class Form1 : Form
    {
        AssemblyName asName = null;
        string exeDir;

        public Form1()
        {
            InitializeComponent();
        }

        private void loadProgramConfig_Click(object sender, EventArgs e)
        {
            try
            {
                asName = AssemblyName.GetAssemblyName(programPathTextBox.Text);
                parametersListBox.Items.Add("Name: " + asName.Name);
                parametersListBox.Items.Add("Full name: " + asName.FullName);
                parametersListBox.Items.Add("Version: " + asName.Version.ToString());
                parametersListBox.Items.Add("Version compatibility: " + asName.VersionCompatibility);
                parametersListBox.Items.Add("Processor architecture: " + asName.ProcessorArchitecture);
                exeDir = Path.GetDirectoryName(programPathTextBox.Text);
                generateReportButton.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private string GenerateReport()
        { 
            string dirName = @"F:\Moj_folder\Dokumenty\F_RS232_Client\f_rs232_client\WebPublisher\logs\" + asName.Version.Major + "." + asName.Version.Minor;
            
            if (!Directory.Exists(dirName))
                Directory.CreateDirectory(dirName);
            
            string filename = dirName + "\\" + asName.Version.ToString();

            StreamWriter sw = new StreamWriter(filename);
            int nr;
            if (alphaRB.Checked)
                nr = 1;
            else if (betaRB.Checked)
                nr = 2;
            else if (releaseRB.Checked)
                nr = 3;
            else
                nr = 4;
            sw.WriteLine(nr.ToString());
            sw.WriteLine(asName.Name);
            sw.WriteLine(asName.FullName);
            sw.WriteLine(asName.ProcessorArchitecture);
            sw.WriteLine(DateTime.Now.Day.ToString() + "/" + DateTime.Now.Month.ToString() + "/" + DateTime.Now.Year.ToString());
            sw.WriteLine(changeLogRichTextBox.Text);

            sw.Close();

            return filename;
        }

        private void CreateFTPDirectory(string directory)
        {
            try
            {
                FtpWebRequest requestDir = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://f-rs232-client.3owl.com/public_html/" + directory));
                requestDir.Method = WebRequestMethods.Ftp.MakeDirectory;
                requestDir.Credentials = new NetworkCredential("u230954128", "1frs232host2");
                requestDir.UsePassive = true;
                requestDir.UseBinary = true;
                requestDir.KeepAlive = false;
                FtpWebResponse response = (FtpWebResponse)requestDir.GetResponse();
                Stream ftpStream = response.GetResponseStream();

                ftpStream.Close();
                response.Close();
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }

        private string ZipEncryption()
        {
            Process p = new Process();
            p.StartInfo.FileName = @"C:\Program Files\7-Zip\7z.exe";
            string cryptedFile = "F:\\Moj_folder\\Dokumenty\\F_RS232_Client\\f_rs232_client\\WebPublisher\\F_RS232_Client.zip";
            File.Delete(cryptedFile);
            p.StartInfo.Arguments = "a \"" + cryptedFile + "\" \"" + exeDir + "\\F_RS232_Client.exe\"";
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.Start();
            p.WaitForExit();

            return cryptedFile;
        }

        private void SendFileToFtp(string fileToSend, string finishFile)
        {
            FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://f-rs232-client.3owl.com/public_html/" + finishFile);
            request.Method = WebRequestMethods.Ftp.UploadFile;

            request.Credentials = new NetworkCredential("u230954128", "1frs232host2");

            byte[] fileContents = File.ReadAllBytes(fileToSend);
            request.ContentLength = fileContents.Length;

            Stream requestStream = request.GetRequestStream();
            requestStream.Write(fileContents, 0, fileContents.Length);
            requestStream.Close();

            FtpWebResponse response = (FtpWebResponse)request.GetResponse();

            MessageBox.Show("Upload File " + fileToSend + "Complete, status:\n " + response.StatusDescription);

            response.Close();
        }

        private void generateReportButton_Click(object sender, EventArgs e)
        {
            try
            {
                string cryptedFile = ZipEncryption();
                string filename = GenerateReport();
                string ftpDir = "logs/"+asName.Version.Major + "." + asName.Version.Minor;
                CreateFTPDirectory(ftpDir);
                SendFileToFtp(filename, ftpDir + "/" + Path.GetFileName(filename));
                SendFileToFtp(cryptedFile, Path.GetFileName(cryptedFile)); 
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
