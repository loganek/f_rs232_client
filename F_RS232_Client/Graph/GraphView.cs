﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ZedGraph;

namespace F_RS232_Client.Graph
{
    public partial class GraphView : Form
    {
        List<GraphDataFrame> gdFrame = new List<GraphDataFrame>();
        List<ComboBox> comboBoxList = new List<ComboBox>();
        List<ColorButton> clrButtonList = new List<ColorButton>();
        DataFrame.Frame dFrame = null;
        ZedGraphControl[] graphs;


        public GraphView()
        {
            InitializeComponent();
            graphCountComboBox.DataSource=new string[]{"1", "2", "4"};
            graphs = new ZedGraphControl[] { z1GraphControl, z2GraphControl, z3GraphControl, z4GraphControl };

            foreach (ZedGraphControl t in graphs)
            {
                GraphPane myPane = t.GraphPane;
                myPane.XAxis.Scale.Min = 0;
                myPane.XAxis.Scale.Max = 100;
                myPane.XAxis.Scale.MinorStep = 1;
                myPane.XAxis.Scale.MajorStep = 5;
                t.AxisChange();
            }
        }

        public void LoadFrame(DataFrame.Frame f)
        {
            dFrame = f;

            for (int i = 0; i < f.ElementCount; i++)
                if (f[i].TypeOf != DataFrame._ElementType.BYTE_ARRAY)
                    gdFrame.Add(new GraphDataFrame(f[i]));

            dFrame.ChangedElementFrame += delegate(object sender, EventArgs e) { DrawVarList(); };
            DrawVarList();
        }

        public void DrawVarList()
        {
            varListPanel.Controls.Clear();
            comboBoxList.Clear();
            int j = 0;

            for (int i = 0; i < dFrame.ElementCount; i++)
            {
                if (dFrame[i].TypeOf == DataFrame._ElementType.BYTE_ARRAY)
                { 
                    comboBoxList.Add(null);
                    clrButtonList.Add(null);
                    continue;
                }

                System.Windows.Forms.Label lbl = new System.Windows.Forms.Label();
                lbl.Text = dFrame[i].TypeOf.ToString() + "  " + dFrame[i].VarName;
                lbl.Location = new Point(10, j * 25 + 10);
                lbl.AutoSize = true;
                varListPanel.Controls.Add(lbl);

                ColorButton clrBtn = new ColorButton();
                clrBtn.Size = new System.Drawing.Size(lbl.Height, lbl.Height);
                clrBtn.Location = new Point(varListPanel.Width - clrBtn.Width - 5, j * 25 + 10);
                clrBtn.SelectedColor = Color.Red;
                clrBtn.Anchor = AnchorStyles.Right | AnchorStyles.Top;
                varListPanel.Controls.Add(clrBtn);
                clrButtonList.Add(clrBtn);

                ComboBox cmbBox = new ComboBox();
                cmbBox.Size = new Size(65, cmbBox.Size.Height);
                cmbBox.Location = new Point(clrBtn.Left - cmbBox.Width - 10, j * 25 + 10);
                cmbBox.DropDownStyle = ComboBoxStyle.DropDownList;
                cmbBox.DataSource = new string[] { "None", "Graph 1", "Graph 2", "Graph 3", "Graph 4" };
                cmbBox.Anchor = AnchorStyles.Right | AnchorStyles.Top;
                varListPanel.Controls.Add(cmbBox);
                comboBoxList.Add(cmbBox);
                j++;
            }
        }

        private void GraphView_FormClosing(object sender, FormClosingEventArgs e)
        {
            dFrame.ChangedElementFrame -= delegate(object s, EventArgs ea) { DrawVarList(); };
        }

        public void pleaseDrawMeNow(object sender, DataFrame.FrameReadyEventArgs e)
        {
            if (InvokeRequired)
                Invoke(new Action(() =>
                {

                    graphs.ToList().ForEach(zgc => zgc.GraphPane.CurveList.Clear());
                    for (int a = 0; a < dFrame.ElementCount; a++)
                    {
                        if (dFrame[a].TypeOf == DataFrame._ElementType.BYTE_ARRAY)
                            continue;

                        if (comboBoxList[a].SelectedIndex == 0)
                            continue;

                        int graphNumber = comboBoxList[a].SelectedIndex - 1;

                        GraphPane myPane = graphs[graphNumber].GraphPane;

                        myPane.XAxis.Title.Text = "X";
                        myPane.YAxis.Title.Text = "Y";

                        double x, y;
                        PointPairList list = new PointPairList();
                        for (int i = 0; i < dFrame[a].Values.Count; i++)
                        {
                            x = (double)(i + dFrame[a].StartIteratorValue());
                            y = Convert.ToDouble(dFrame[a].Values[i]);
                            list.Add(x, y);
                        }

                        Scale xScale = graphs[graphNumber].GraphPane.XAxis.Scale;
                        xScale.Min = dFrame[a].StartIteratorValue();
                        xScale.Max = dFrame[a].StartIteratorValue() + 100;
                        myPane.AddCurve(dFrame[a].VarName, list, clrButtonList[a].SelectedColor, SymbolType.None);

                        graphs[graphNumber].AxisChange();
                        graphs[graphNumber].Refresh();
                    }
                }));
        }

        private void graphCountComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (graphCountComboBox.SelectedIndex)
            { 
                case 0:
                    mainSplitContainer.Panel2Collapsed = true;
                    leftSplitContainer.Panel2Collapsed = true;

                    break;
                case 1:
                    mainSplitContainer.Panel2Collapsed = true;
                    leftSplitContainer.Panel2Collapsed = false;
                    break;
                case 2:
                    mainSplitContainer.Panel2Collapsed = false;
                    leftSplitContainer.Panel2Collapsed = false;
                    break;

            }
        }
    }
}
