﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace F_RS232_Client.Options
{
    public class ConfigManager
    {
        string fileName;
        bool isLoaded = false;
        Dictionary<string, string> dicConfig = new Dictionary<string, string>();

        public ConfigManager(string fName)
        {
            fileName = fName;
        }

        public void LoadConfig()
        {
            if (!File.Exists(fileName))
                return;

            StreamReader sr = new StreamReader(fileName);
            string tmpLine;
            int fIndex;

            while ((tmpLine = sr.ReadLine()) != null)
            {
                tmpLine = tmpLine.Replace(" ", "");
                fIndex = tmpLine.IndexOf('=');

                if (fIndex < 1)
                    continue;

                dicConfig.Add(tmpLine.Substring(0, fIndex).ToLower(), tmpLine.Substring(fIndex + 1));
            }

            sr.Close();
            isLoaded = true;
        }

        public void ResetConfig()
        {
            using (FileStream stream = new FileStream(fileName, FileMode.Create)) 
                dicConfig.Clear();
        }

        public void SaveConfig()
        {
            StreamWriter sw = new StreamWriter(fileName);

            foreach (KeyValuePair<string, string> pair in dicConfig)
                sw.WriteLine(pair.Key.ToLower() + "=" + pair.Value);

            sw.Close();
        }

        public string this[string key]
        {
            get
            {
                key = key.ToLower();

                if (IsKey(key))
                    return dicConfig[key];
                else
                    return null;
            }

            set
            {
                key = key.ToLower();

                if (IsKey(key))
                    dicConfig[key] = value;
                else
                    dicConfig.Add(key, value);
            }
        }

        public int GetInt(string key)
        {
            return Convert.ToInt32(this[key]);
        }

        public bool IsKey(string key)
        {
            return dicConfig.ContainsKey(key.ToLower());
        }

        public bool IsLoaded()
        {
            return isLoaded;
        }
    }
}
