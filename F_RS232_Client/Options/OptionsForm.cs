﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace F_RS232_Client
{
    public partial class OptionsForm : Form
    {
        Options.ConfigManager cfgManager = null;

        public OptionsForm()
        {
            InitializeComponent();
            recUpdateDataIntervalTextBox.DataBindings.Add("Enabled", recIntervalUpdateRadioButton, "Checked");
            sendUpdateDataIntervalTextBox.DataBindings.Add("Enabled", sendIntervalUpdateRadioButton, "Checked");
        }

        private void optionListTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Action == TreeViewAction.ByMouse)
            {
                if (e.Node.Name == "generalNode")
                {
                    mainTabPage.SelectedTab = mainTabPage.TabPages["generalTab"];
                }
                else if (e.Node.Name == "dataUpdateNode")
                {
                    mainTabPage.SelectedTab = mainTabPage.TabPages["dataUpdateTab"];
                }
            }
        }

        private void macroDefaultFileButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            { 
                if(System.IO.File.Exists(openFileDialog.FileName))
                {
                    defaultMacroFileTextBox.Text = openFileDialog.FileName;
                }
            }
        }

        private void frameDefaultFileButton_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (System.IO.File.Exists(openFileDialog.FileName))
                {
                    defaultFrameFileTextBox.Text = openFileDialog.FileName;
                }
            }
        }

        private void saveConfigButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (sendIntervalUpdateRadioButton.Checked)
                {
                    uint timeSend = Convert.ToUInt32(sendUpdateDataIntervalTextBox.Text);

                    if (timeSend <= 0)
                        throw new Exception("Interval for sending must be greater than 0");
                    cfgManager["SendUpdateDataInterval"] = timeSend.ToString();
                }

                if (recIntervalUpdateRadioButton.Checked)
                {
                    uint timeRec = Convert.ToUInt32(recUpdateDataIntervalTextBox.Text);

                    if (timeRec <= 0)
                        throw new Exception("Interval must be greater than 0");
                    cfgManager["RecUpdateDataInterval"] = timeRec.ToString();
                }

                cfgManager["FrameDefaultFile"] = defaultFrameFileTextBox.Text;
                cfgManager["MacroDefaultFile"] = defaultMacroFileTextBox.Text;
                
                cfgManager["RecTimeoutUpdate"] = recIntervalUpdateRadioButton.Checked.ToString();
                cfgManager["SendTimeoutUpdate"] = sendIntervalUpdateRadioButton.Checked.ToString();
                
                cfgManager.SaveConfig();

                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot save config: " + ex.Message);
            }
        }

        public void LoadConfig(Options.ConfigManager cfgM)
        {
            cfgManager = cfgM;
            defaultFrameFileTextBox.Text = cfgManager["FrameDefaultFile"];
            defaultMacroFileTextBox.Text = cfgManager["MacroDefaultFile"];
            recUpdateDataIntervalTextBox.Text = cfgManager["RecUpdateDataInterval"];

            if (Convert.ToBoolean(cfgManager["RecTimeoutUpdate"]))
                recIntervalUpdateRadioButton.Checked = true;
            else
                recUpdateImmediatelyRadioButton.Checked = true;

            sendUpdateDataIntervalTextBox.Text = cfgManager["SendUpdateDataInterval"];

            if (Convert.ToBoolean(cfgManager["SendTimeoutUpdate"]))
                sendIntervalUpdateRadioButton.Checked = true;
            else
                sendUpdateImmediatelyRadioButton.Checked = true;
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        public Options.ConfigManager CfgManager
        {
            get { return cfgManager; }
        }
    }

    public partial class TabControlWithoutHeader : TabControl
    {
        public TabControlWithoutHeader()
            :base()
        {
            
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0x1328 && !DesignMode)
                m.Result = (IntPtr)1;
            else
                base.WndProc(ref m);
        }
    }
}
