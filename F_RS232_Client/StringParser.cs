﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace F_RS232_Client
{
    class StringParser
    {
        static Dictionary<char, byte> spCHArray = new Dictionary<char, byte>();

        static StringParser()
        {
            spCHArray.Add('n', 10);
            spCHArray.Add('r', 13);
            spCHArray.Add('0', 9);
            spCHArray.Add('\\', 92);
        }

        public static byte SpecialCharacter(char sign)
        {
            if (spCHArray.ContainsKey(sign))
                return spCHArray[sign];
            else
                throw new Exception("Cannot find special character: " + sign);
        }

        /// <summary>
        /// parsuje string na tablice bajtów
        /// \xXX zamienia XX na bajt. XX - zapis szesnastkowy
        /// \dXX zamienia XX na bajt. XX - zapis dziesiętny
        /// </summary>
        /// <param name="str">ciąg do sparsowania</param>
        /// <returns>tablica bajtów po sparsowaniu</returns>
        public static byte[] StrToByteArray(string str)
        {
            List<byte> conBytes = new List<byte>();

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '\\')
                {
                    if (i + 1 == str.Length)
                        throw new Exception("Cannot parse string");

                    if (str[i + 1] == 'x')
                    {
                        if (i + 3 == str.Length)
                            throw new Exception("Cannot parse string");

                        conBytes.Add(Convert.ToByte(str.Substring(i + 2, 2), 16));
                        i += 3;
                    }
                    else if (str[i + 1] == 'd')
                    {
                        if (i + 4 == str.Length)
                            throw new Exception("Cannot parse string");

                        conBytes.Add(Convert.ToByte(str.Substring(i + 2, 3)));
                        i += 4;
                    }
                    else
                        conBytes.Add(SpecialCharacter(str[++i]));

                }
                else
                    conBytes.Add(Convert.ToByte(str[i]));
            }

            return conBytes.ToArray();
        }

        public static string ByteArrayToStr(byte[] bArr)
        {
            if (bArr == null)
                return "";

            string ret = "";

            for (int i = 0; i < bArr.Length; i++)
                ret += (bArr[i] >= 32 && bArr[i] <= 126) ? Convert.ToChar(bArr[i]).ToString() : "\\x" + bArr[i].ToString("X2");

            return ret;
        }

        //public static string NonVisibleByteToDisplay(byte[] byteArray, int p = -1)
        //{
        //    string retText = "";

        //    for (int i = 0; i < byteArray.Length; i++)
        //    {
        //        if (byteArray[i] > 31 && byteArray[i] < 128 || p == -1)
        //            retText += Convert.ToChar(byteArray[i]);

        //        else
        //            retText += "{x" + byteArray[i].ToString("X2") + "}";
        //    }

        //    return retText;
        //}

        public static string ByteToDisplay(byte[] byteArray, int p = -1)
        {
            string retText = "";

            for (int i = 0; i < byteArray.Length; i++)
            {
                if (p == 16)
                    retText += "{x" + byteArray[i].ToString("X2") + "}";

                else
                    retText += Convert.ToChar(byteArray[i]);
            }

            return retText;
        }
    }
}
