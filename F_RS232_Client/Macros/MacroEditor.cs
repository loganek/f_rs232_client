﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace F_RS232_Client.Macros
{
    public partial class MacroEditor : Form
    {
        private MacroList realList = null;
        private MacroList mList = new MacroList();
        List<TextBox> txtBoxNameList = new List<TextBox>();
        List<TextBox> txtBoxDescList = new List<TextBox>();

        public MacroEditor()
        {
            InitializeComponent();
        }

        public void LoadList(MacroList mL)
        {
            realList = mL;
        }

        public void DrawMacroComponents(int i)
        {
            // name textbox
            TextBox txtBox = new TextBox();
            txtBox.Location = new Point(10, (txtBox.Height + 10) * i + 10);
            txtBox.Size = new Size(60, txtBox.Size.Height);
            txtBox.Text = mList[i].Name;
            txtBox.ReadOnly = true;
            txtBoxNameList.Add(txtBox);
            mainPanel.Controls.Add(txtBox);

            // description textbox
            txtBox = new TextBox();
            txtBox.Location = new Point(88, (txtBox.Height + 10) * i + 10);
            txtBox.Size = new Size(100, txtBox.Size.Height);
            txtBox.Text = mList[i].Content;
            txtBox.ReadOnly = true;
            txtBoxDescList.Add(txtBox);
            mainPanel.Controls.Add(txtBox);

            //remove button
            Button btn = new Button();
            btn.Size = new Size(addMacroButton.Width - 15, txtBox.Height);
            btn.Location = new Point(addMacroButton.Left, txtBox.Top);
            btn.Text = "Remove";
            btn.Tag = i.ToString();
            btn.Click += removeBtn_Click;
            mainPanel.Controls.Add(btn);
        }

        public void DrawMacroEditor()
        {
            while (mainPanel.Controls.Count > 0)
                mainPanel.Controls.RemoveAt(0);

            if (realList == null)
                return;

            for (int i = 0; i < mList.Count; i++)
            {
                DrawMacroComponents(i);
            }
        }

        public void UpdateMacroEditor(int index)
        {
            for (int i = 0; i < 3; i++)
                mainPanel.Controls.RemoveAt(index * 3);

            int hof = new TextBox().Height;

            for (int i = index; i < mainPanel.Controls.Count / 3; i++)
            {
                for (int j = 0; j < 3; j++)
                    mainPanel.Controls[i * 3 + j].Top = (hof + 10) * i + 10;

                mainPanel.Controls[i * 3 + 2].Tag = i;
            }
        }

        private void removeBtn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            
            if (btn != null)
            {
                try
                {
                    int index = Convert.ToInt32(btn.Tag);

                    mList.RemoveMacro(txtBoxNameList[index].Text);
                    txtBoxDescList.RemoveAt(index);
                    txtBoxNameList.RemoveAt(index);
                    UpdateMacroEditor(index);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot remove macro: " + ex.Message);
                }
            }
        }

        private void addMacroButton_Click(object sender, EventArgs e)
        {
            try
            {
                mList.AddMacro(macroDescriptionTextBox.Text, macroContentTextBox.Text);
                macroDescriptionTextBox.Clear();
                macroContentTextBox.Clear();
                DrawMacroComponents(mList.Count - 1);
                macroDescriptionTextBox.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot add macro: " + ex.Message);
            }
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < mList.Count; i++)
            {
                mList[i].Content = txtBoxDescList[i].Text;
                mList[i].Name = txtBoxNameList[i].Text;
            }

            realList.SetNewList(mList);
            this.Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MacroEditor_VisibleChanged(object sender, EventArgs e)
        {
            mList.SetNewList(realList);
            DrawMacroEditor();
        }

        private void macroDescriptionTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Return)
                addMacroButton_Click(this, null);
        }

        private void loadMacrosButton_Click(object sender, EventArgs e)
        {
            if (loadMacrosFileDialog.ShowDialog() == DialogResult.OK)
            {
                if (System.IO.File.Exists(loadMacrosFileDialog.FileName))
                {
                    mList.LoadFromFile(loadMacrosFileDialog.FileName);
                    DrawMacroEditor();
                }
                else
                    MessageBox.Show("File doesn't exist");
            }
        }
        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape) 
                this.Close();
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void saveMacrosButton_Click(object sender, EventArgs e)
        {
            if (saveMacrosFileDialog.ShowDialog() == DialogResult.OK)
                mList.SaveToFile(saveMacrosFileDialog.FileName);
        }
    }
}
