﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace F_RS232_Client
{
    enum LogType { NormalLog, ErrorLog, HappyLog };

    class MessageLogger
    {
        RichTextBox rtBox;

        public MessageLogger(RichTextBox rtBox)
        {
            this.rtBox = rtBox;
        }

        public void WriteLog(string text, LogType lType = LogType.NormalLog)
        {
            Color clr;

            switch (lType)
            {
                case LogType.ErrorLog:
                    clr = Color.Red;
                    break;
                case LogType.HappyLog:
                    clr = Color.DarkGreen;
                    break;
                default:
                case LogType.NormalLog:
                    clr = Color.FromArgb(70, 70, 70);
                    break;
            }

            rtBox.AppendText("\n" + text + "\n", clr);
        }
    }

    public static class RichTextBoxExtensions
    {
        public static void AppendText(this RichTextBox box, string text, Color color)
        {
            Action act = new Action(() =>
            {
                box.SelectionStart = box.TextLength;
                box.SelectionLength = 0;

                box.SelectionColor = color;
                box.AppendText(text);
                box.SelectionColor = box.ForeColor;
                box.ScrollToCaret();
            });

            if (box.InvokeRequired)
                box.Invoke(act);
            else
                act.Invoke();
                
        }
    }
}
