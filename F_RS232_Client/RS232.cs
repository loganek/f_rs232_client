﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;

namespace F_RS232_Client
{
    public class SentEventArgs : EventArgs
    {
        private byte[] buffer;

        public byte[] Buff
        {
            get { return buffer; }
        }

        public SentEventArgs(byte[] buff)
        {
            if (buff == null)
                buffer = null;
            else
            {
                buffer = new byte[buff.Length];
                Buffer.BlockCopy(buff, 0, buffer, 0, buffer.Length);
            }
        }
    }

    public delegate void ConnectedEventHandler(object sender, EventArgs e);
    public delegate void DisconnectedEventHandler(object sender, EventArgs e);
    public delegate void DataReceivedEventHandler(object sender, EventArgs e);
    public delegate void DataSentEventHandler(object sender, SentEventArgs e);

    public class RS232
    {
        SerialPort port;
        string portName;
        int baudRate;
        Parity parity;
        int dataBits;
        StopBits stopBits;
        byte[] rsData;
        //Thread rsThread;

        static Parity[] parityArray = { Parity.None, Parity.Odd, Parity.Even, Parity.Mark, Parity.Space };
        static StopBits[] stopBitsArray = { StopBits.One, StopBits.OnePointFive, StopBits.Two };

        public static Parity ParityArray(int index)
        {
            return parityArray[index];
        }

        public static StopBits StopBitsArray(int index)
        {
            return stopBitsArray[index];
        }

        public byte[] RsData
        {
            get { return rsData; }
        }

        public static string[] GetAvaliablePorts()
        {
            return SerialPort.GetPortNames();
        }

        public bool IsConnected
        {
            get { return (port != null && port.IsOpen) ? true : false; }
        }

        public string PortName
        {
            set { portName = value; }
            get { return portName; }
        }

        public StopBits StopBits
        {
            set { stopBits = value; }
            get { return stopBits; }
        }

        public int BaudRate
        {
            set { baudRate = value; }
            get { return baudRate; }
        }

        public Parity Parity
        {
            set { parity = value; }
            get { return parity; }
        }

        public int DataBits
        {
            set { dataBits = value; }
            get { return dataBits; }
        }

        public RS232() 
        { }

        public void SetConnectionParameters(string portName, int baudRate, Parity parity = Parity.None, int dataBits = 8, StopBits stopBits = StopBits.None)
        {
            this.portName = portName;
            this.baudRate = baudRate;
            this.parity = parity;
            this.dataBits = dataBits;
            this.stopBits = stopBits;
        }

        public void ReceiveData()
        {
            while (port.IsOpen)
            {
                int toRead;
                if ((toRead = port.BytesToRead) > 0)
                {
                    rsData = new byte[toRead];

                    port.Read(rsData, 0, toRead);

                    OnDataReceived(EventArgs.Empty);
                }
            }
        }


        public void port_DataReceived(object sender, EventArgs e)
        {
            int toRead = port.BytesToRead;
            rsData = new byte[toRead];

            port.Read(rsData, 0, toRead);

            OnDataReceived(EventArgs.Empty);
        }

        public void Connect()
        {
            port = new SerialPort(portName, baudRate, parity, dataBits, stopBits);
            port.DataReceived += new SerialDataReceivedEventHandler(port_DataReceived);

            if (port.IsOpen)
                return;

            try
            {
                //rsThread = new Thread(ReceiveData);
                //rsThread.IsBackground = true;
                port.Open();
                //rsThread.Start();
                OnConnected(EventArgs.Empty);
            }
            catch
            {
                throw new Exception("Probably port opened in another program");
            }
        }

        public bool Disconnect()
        {
            if (!port.IsOpen)
                return true;

            try
            {
                //rsThread.Abort();
                port.BreakState = true;
                port.DtrEnable = false;
                port.RtsEnable = false;
                port.DiscardInBuffer();
                port.DiscardOutBuffer();
                port.Close();
                OnDisconnected(EventArgs.Empty);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Send(byte[] buf)
        {
            if (buf == null)
                return true;

            port.Write(buf, 0, buf.Length);
            OnDataSent(new SentEventArgs(buf));
            return true;
        }

#region events
        public event ConnectedEventHandler Connected;
        public event DisconnectedEventHandler Disconnected;
        public event DataReceivedEventHandler DataReceived;
        public event DataSentEventHandler DataSent;

        protected virtual void OnConnected(EventArgs e)
        {
            if (Connected != null)
                Connected(this, e);
        }

        protected virtual void OnDisconnected(EventArgs e)
        {
            if (Disconnected != null)
                Disconnected(this, e);
        }

        protected virtual void OnDataReceived(EventArgs e)
        {
            if (DataReceived != null)
                DataReceived(this, e);
        }

        protected virtual void OnDataSent(SentEventArgs e)
        {
            if (DataSent != null)
                DataSent(this, e);
        }
#endregion
    }
}
