﻿namespace WebPublisher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.changeLogRichTextBox = new System.Windows.Forms.RichTextBox();
            this.programPathTextBox = new System.Windows.Forms.TextBox();
            this.parametersListBox = new System.Windows.Forms.ListBox();
            this.loadProgramConfig = new System.Windows.Forms.Button();
            this.generateReportButton = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.alphaRB = new System.Windows.Forms.RadioButton();
            this.betaRB = new System.Windows.Forms.RadioButton();
            this.releaseRB = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // changeLogRichTextBox
            // 
            this.changeLogRichTextBox.Location = new System.Drawing.Point(336, 27);
            this.changeLogRichTextBox.Name = "changeLogRichTextBox";
            this.changeLogRichTextBox.Size = new System.Drawing.Size(301, 95);
            this.changeLogRichTextBox.TabIndex = 0;
            this.changeLogRichTextBox.Text = "";
            // 
            // programPathTextBox
            // 
            this.programPathTextBox.Location = new System.Drawing.Point(0, 1);
            this.programPathTextBox.Name = "programPathTextBox";
            this.programPathTextBox.Size = new System.Drawing.Size(556, 20);
            this.programPathTextBox.TabIndex = 1;
            this.programPathTextBox.Text = "F:\\Moj_folder\\Dokumenty\\F_RS232_Client\\f_rs232_client\\F_RS232_Client\\bin\\Release\\" +
    "F_RS232_Client.exe";
            // 
            // parametersListBox
            // 
            this.parametersListBox.FormattingEnabled = true;
            this.parametersListBox.HorizontalScrollbar = true;
            this.parametersListBox.Location = new System.Drawing.Point(0, 27);
            this.parametersListBox.Name = "parametersListBox";
            this.parametersListBox.Size = new System.Drawing.Size(330, 95);
            this.parametersListBox.TabIndex = 2;
            // 
            // loadProgramConfig
            // 
            this.loadProgramConfig.Location = new System.Drawing.Point(562, 1);
            this.loadProgramConfig.Name = "loadProgramConfig";
            this.loadProgramConfig.Size = new System.Drawing.Size(75, 23);
            this.loadProgramConfig.TabIndex = 3;
            this.loadProgramConfig.Text = "Load";
            this.loadProgramConfig.UseVisualStyleBackColor = true;
            this.loadProgramConfig.Click += new System.EventHandler(this.loadProgramConfig_Click);
            // 
            // generateReportButton
            // 
            this.generateReportButton.Enabled = false;
            this.generateReportButton.Location = new System.Drawing.Point(224, 144);
            this.generateReportButton.Name = "generateReportButton";
            this.generateReportButton.Size = new System.Drawing.Size(106, 23);
            this.generateReportButton.TabIndex = 4;
            this.generateReportButton.Text = "Generate report";
            this.generateReportButton.UseVisualStyleBackColor = true;
            this.generateReportButton.Click += new System.EventHandler(this.generateReportButton_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.releaseRB);
            this.groupBox1.Controls.Add(this.betaRB);
            this.groupBox1.Controls.Add(this.alphaRB);
            this.groupBox1.Location = new System.Drawing.Point(437, 128);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 40);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Version";
            // 
            // alphaRB
            // 
            this.alphaRB.AutoSize = true;
            this.alphaRB.Checked = true;
            this.alphaRB.Location = new System.Drawing.Point(3, 16);
            this.alphaRB.Name = "alphaRB";
            this.alphaRB.Size = new System.Drawing.Size(52, 17);
            this.alphaRB.TabIndex = 0;
            this.alphaRB.TabStop = true;
            this.alphaRB.Text = "Alpha";
            this.alphaRB.UseVisualStyleBackColor = true;
            // 
            // betaRB
            // 
            this.betaRB.AutoSize = true;
            this.betaRB.Location = new System.Drawing.Point(61, 16);
            this.betaRB.Name = "betaRB";
            this.betaRB.Size = new System.Drawing.Size(47, 17);
            this.betaRB.TabIndex = 1;
            this.betaRB.Text = "Beta";
            this.betaRB.UseVisualStyleBackColor = true;
            // 
            // releaseRB
            // 
            this.releaseRB.AutoSize = true;
            this.releaseRB.Location = new System.Drawing.Point(114, 16);
            this.releaseRB.Name = "releaseRB";
            this.releaseRB.Size = new System.Drawing.Size(64, 17);
            this.releaseRB.TabIndex = 2;
            this.releaseRB.Text = "Release";
            this.releaseRB.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(638, 176);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.generateReportButton);
            this.Controls.Add(this.loadProgramConfig);
            this.Controls.Add(this.parametersListBox);
            this.Controls.Add(this.programPathTextBox);
            this.Controls.Add(this.changeLogRichTextBox);
            this.Name = "Form1";
            this.Text = "WebPublisher";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox changeLogRichTextBox;
        private System.Windows.Forms.TextBox programPathTextBox;
        private System.Windows.Forms.ListBox parametersListBox;
        private System.Windows.Forms.Button loadProgramConfig;
        private System.Windows.Forms.Button generateReportButton;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton releaseRB;
        private System.Windows.Forms.RadioButton betaRB;
        private System.Windows.Forms.RadioButton alphaRB;
    }
}

