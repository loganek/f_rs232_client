﻿namespace F_RS232_Client
{
    partial class OptionsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("General");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Data updating");
            this.optionListTreeView = new System.Windows.Forms.TreeView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.saveConfigButton = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.mainTabPage = new F_RS232_Client.TabControlWithoutHeader();
            this.generalTab = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.defaultFrameFileTextBox = new System.Windows.Forms.TextBox();
            this.frameDefaultFileButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.defaultMacroFileTextBox = new System.Windows.Forms.TextBox();
            this.macroDefaultFileButton = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.dataUpdateTab = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sendUpdateImmediatelyRadioButton = new System.Windows.Forms.RadioButton();
            this.sendIntervalUpdateRadioButton = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.sendUpdateDataIntervalTextBox = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.recUpdateImmediatelyRadioButton = new System.Windows.Forms.RadioButton();
            this.recIntervalUpdateRadioButton = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.recUpdateDataIntervalTextBox = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.mainTabPage.SuspendLayout();
            this.generalTab.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.dataUpdateTab.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // optionListTreeView
            // 
            this.optionListTreeView.Dock = System.Windows.Forms.DockStyle.Left;
            this.optionListTreeView.Location = new System.Drawing.Point(0, 0);
            this.optionListTreeView.Name = "optionListTreeView";
            treeNode1.Name = "generalNode";
            treeNode1.Text = "General";
            treeNode2.Name = "dataUpdateNode";
            treeNode2.Text = "Data updating";
            this.optionListTreeView.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2});
            this.optionListTreeView.Size = new System.Drawing.Size(121, 312);
            this.optionListTreeView.TabIndex = 0;
            this.optionListTreeView.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.optionListTreeView_AfterSelect);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.saveConfigButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 312);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(386, 35);
            this.panel1.TabIndex = 2;
            // 
            // cancelButton
            // 
            this.cancelButton.Location = new System.Drawing.Point(304, 6);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // saveConfigButton
            // 
            this.saveConfigButton.Location = new System.Drawing.Point(223, 6);
            this.saveConfigButton.Name = "saveConfigButton";
            this.saveConfigButton.Size = new System.Drawing.Size(75, 23);
            this.saveConfigButton.TabIndex = 0;
            this.saveConfigButton.Text = "OK";
            this.saveConfigButton.UseVisualStyleBackColor = true;
            this.saveConfigButton.Click += new System.EventHandler(this.saveConfigButton_Click);
            // 
            // mainTabPage
            // 
            this.mainTabPage.Controls.Add(this.generalTab);
            this.mainTabPage.Controls.Add(this.dataUpdateTab);
            this.mainTabPage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainTabPage.Location = new System.Drawing.Point(121, 0);
            this.mainTabPage.Name = "mainTabPage";
            this.mainTabPage.SelectedIndex = 0;
            this.mainTabPage.Size = new System.Drawing.Size(265, 312);
            this.mainTabPage.TabIndex = 1;
            // 
            // generalTab
            // 
            this.generalTab.Controls.Add(this.groupBox3);
            this.generalTab.Controls.Add(this.groupBox2);
            this.generalTab.Location = new System.Drawing.Point(4, 22);
            this.generalTab.Name = "generalTab";
            this.generalTab.Padding = new System.Windows.Forms.Padding(3);
            this.generalTab.Size = new System.Drawing.Size(257, 286);
            this.generalTab.TabIndex = 0;
            this.generalTab.Text = "tabPage1";
            this.generalTab.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.defaultFrameFileTextBox);
            this.groupBox3.Controls.Add(this.frameDefaultFileButton);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Location = new System.Drawing.Point(3, 61);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(258, 47);
            this.groupBox3.TabIndex = 3;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Load frame on startup";
            // 
            // defaultFrameFileTextBox
            // 
            this.defaultFrameFileTextBox.Location = new System.Drawing.Point(64, 19);
            this.defaultFrameFileTextBox.Name = "defaultFrameFileTextBox";
            this.defaultFrameFileTextBox.Size = new System.Drawing.Size(158, 20);
            this.defaultFrameFileTextBox.TabIndex = 2;
            // 
            // frameDefaultFileButton
            // 
            this.frameDefaultFileButton.Location = new System.Drawing.Point(228, 16);
            this.frameDefaultFileButton.Name = "frameDefaultFileButton";
            this.frameDefaultFileButton.Size = new System.Drawing.Size(26, 23);
            this.frameDefaultFileButton.TabIndex = 1;
            this.frameDefaultFileButton.Text = "...";
            this.frameDefaultFileButton.UseVisualStyleBackColor = true;
            this.frameDefaultFileButton.Click += new System.EventHandler(this.frameDefaultFileButton_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "File name:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.defaultMacroFileTextBox);
            this.groupBox2.Controls.Add(this.macroDefaultFileButton);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(3, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 49);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Load macro file on startup";
            // 
            // defaultMacroFileTextBox
            // 
            this.defaultMacroFileTextBox.Location = new System.Drawing.Point(64, 19);
            this.defaultMacroFileTextBox.Name = "defaultMacroFileTextBox";
            this.defaultMacroFileTextBox.Size = new System.Drawing.Size(158, 20);
            this.defaultMacroFileTextBox.TabIndex = 2;
            // 
            // macroDefaultFileButton
            // 
            this.macroDefaultFileButton.Location = new System.Drawing.Point(228, 16);
            this.macroDefaultFileButton.Name = "macroDefaultFileButton";
            this.macroDefaultFileButton.Size = new System.Drawing.Size(26, 23);
            this.macroDefaultFileButton.TabIndex = 1;
            this.macroDefaultFileButton.Text = "...";
            this.macroDefaultFileButton.UseVisualStyleBackColor = true;
            this.macroDefaultFileButton.Click += new System.EventHandler(this.macroDefaultFileButton_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "File name:";
            // 
            // dataUpdateTab
            // 
            this.dataUpdateTab.Controls.Add(this.groupBox1);
            this.dataUpdateTab.Controls.Add(this.groupBox4);
            this.dataUpdateTab.Location = new System.Drawing.Point(4, 22);
            this.dataUpdateTab.Name = "dataUpdateTab";
            this.dataUpdateTab.Padding = new System.Windows.Forms.Padding(3);
            this.dataUpdateTab.Size = new System.Drawing.Size(257, 286);
            this.dataUpdateTab.TabIndex = 1;
            this.dataUpdateTab.Text = "tabPage2";
            this.dataUpdateTab.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sendUpdateImmediatelyRadioButton);
            this.groupBox1.Controls.Add(this.sendIntervalUpdateRadioButton);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.sendUpdateDataIntervalTextBox);
            this.groupBox1.Location = new System.Drawing.Point(3, 94);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 82);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sended data";
            // 
            // sendUpdateImmediatelyRadioButton
            // 
            this.sendUpdateImmediatelyRadioButton.AutoSize = true;
            this.sendUpdateImmediatelyRadioButton.Checked = true;
            this.sendUpdateImmediatelyRadioButton.Location = new System.Drawing.Point(6, 19);
            this.sendUpdateImmediatelyRadioButton.Name = "sendUpdateImmediatelyRadioButton";
            this.sendUpdateImmediatelyRadioButton.Size = new System.Drawing.Size(141, 30);
            this.sendUpdateImmediatelyRadioButton.TabIndex = 3;
            this.sendUpdateImmediatelyRadioButton.TabStop = true;
            this.sendUpdateImmediatelyRadioButton.Text = "Update data immediately\nwhen data occurs";
            this.sendUpdateImmediatelyRadioButton.UseVisualStyleBackColor = true;
            // 
            // sendIntervalUpdateRadioButton
            // 
            this.sendIntervalUpdateRadioButton.AutoSize = true;
            this.sendIntervalUpdateRadioButton.Location = new System.Drawing.Point(165, 19);
            this.sendIntervalUpdateRadioButton.Name = "sendIntervalUpdateRadioButton";
            this.sendIntervalUpdateRadioButton.Size = new System.Drawing.Size(84, 30);
            this.sendIntervalUpdateRadioButton.TabIndex = 4;
            this.sendIntervalUpdateRadioButton.Text = "Update data\nwith interval";
            this.sendIntervalUpdateRadioButton.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(60, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Interval[ms]:";
            // 
            // sendUpdateDataIntervalTextBox
            // 
            this.sendUpdateDataIntervalTextBox.Location = new System.Drawing.Point(130, 54);
            this.sendUpdateDataIntervalTextBox.Name = "sendUpdateDataIntervalTextBox";
            this.sendUpdateDataIntervalTextBox.Size = new System.Drawing.Size(69, 20);
            this.sendUpdateDataIntervalTextBox.TabIndex = 1;
            this.sendUpdateDataIntervalTextBox.Text = "1000";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.recUpdateImmediatelyRadioButton);
            this.groupBox4.Controls.Add(this.recIntervalUpdateRadioButton);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Controls.Add(this.recUpdateDataIntervalTextBox);
            this.groupBox4.Location = new System.Drawing.Point(3, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(258, 82);
            this.groupBox4.TabIndex = 6;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Recieved data";
            // 
            // recUpdateImmediatelyRadioButton
            // 
            this.recUpdateImmediatelyRadioButton.AutoSize = true;
            this.recUpdateImmediatelyRadioButton.Checked = true;
            this.recUpdateImmediatelyRadioButton.Location = new System.Drawing.Point(6, 19);
            this.recUpdateImmediatelyRadioButton.Name = "recUpdateImmediatelyRadioButton";
            this.recUpdateImmediatelyRadioButton.Size = new System.Drawing.Size(141, 30);
            this.recUpdateImmediatelyRadioButton.TabIndex = 3;
            this.recUpdateImmediatelyRadioButton.TabStop = true;
            this.recUpdateImmediatelyRadioButton.Text = "Update data immediately\nwhen data occurs";
            this.recUpdateImmediatelyRadioButton.UseVisualStyleBackColor = true;
            // 
            // recIntervalUpdateRadioButton
            // 
            this.recIntervalUpdateRadioButton.AutoSize = true;
            this.recIntervalUpdateRadioButton.Location = new System.Drawing.Point(165, 19);
            this.recIntervalUpdateRadioButton.Name = "recIntervalUpdateRadioButton";
            this.recIntervalUpdateRadioButton.Size = new System.Drawing.Size(84, 30);
            this.recIntervalUpdateRadioButton.TabIndex = 4;
            this.recIntervalUpdateRadioButton.Text = "Update data\nwith interval";
            this.recIntervalUpdateRadioButton.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(60, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Interval[ms]:";
            // 
            // recUpdateDataIntervalTextBox
            // 
            this.recUpdateDataIntervalTextBox.Location = new System.Drawing.Point(130, 54);
            this.recUpdateDataIntervalTextBox.Name = "recUpdateDataIntervalTextBox";
            this.recUpdateDataIntervalTextBox.Size = new System.Drawing.Size(69, 20);
            this.recUpdateDataIntervalTextBox.TabIndex = 1;
            this.recUpdateDataIntervalTextBox.Text = "1000";
            // 
            // OptionsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(386, 347);
            this.Controls.Add(this.mainTabPage);
            this.Controls.Add(this.optionListTreeView);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "OptionsForm";
            this.Text = "Options";
            this.panel1.ResumeLayout(false);
            this.mainTabPage.ResumeLayout(false);
            this.generalTab.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.dataUpdateTab.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TreeView optionListTreeView;
        private TabControlWithoutHeader mainTabPage;
        private System.Windows.Forms.TabPage generalTab;
        private System.Windows.Forms.TabPage dataUpdateTab;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button saveConfigButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button macroDefaultFileButton;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.TextBox defaultMacroFileTextBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox defaultFrameFileTextBox;
        private System.Windows.Forms.Button frameDefaultFileButton;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton sendUpdateImmediatelyRadioButton;
        private System.Windows.Forms.RadioButton sendIntervalUpdateRadioButton;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox sendUpdateDataIntervalTextBox;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.RadioButton recUpdateImmediatelyRadioButton;
        private System.Windows.Forms.RadioButton recIntervalUpdateRadioButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox recUpdateDataIntervalTextBox;
    }
}